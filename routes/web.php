<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth.login');
// });

Auth::routes();

Route::namespace('Admin')->group(function () {
    Route::prefix('admin')->group(function () {
        Route::middleware('auth')->group(function () {
            Route::name('admin.')->group(function () {

                Route::prefix('users')->group(function () {
                    Route::name('users.')->group(function () {
                        Route::get('/', 'UserController@index')->name('index');
                        Route::get('/store', 'UserController@add')->name('add');
                        Route::post('/store', 'UserController@store')->name('store');
                        Route::get('/detail/{user}', 'UserController@detail')->name('edit');
                        Route::put('/update/{user}', 'UserController@update')->name('update');
                        Route::get('/edit-password/{user}', 'UserController@editPassword')->name('edit.password');
                        Route::put('/edit-password/{user}', 'UserController@updatePassword')->name('update.password');
                        Route::delete('/delete/{user}', 'UserController@delete')->name('delete');
                    });
                });

                Route::prefix('categories')->group(function () {
                    Route::name('categories.')->group(function () {
                        Route::get('/', 'CategoryController@index')->name('index');
                        Route::get('/store', 'CategoryController@add')->name('add');
                        Route::post('/store', 'CategoryController@store')->name('store');
                        Route::get('/detail/{category}', 'CategoryController@detail')->name('edit');
                        Route::put('/update/{category}', 'CategoryController@update')->name('update');
                        Route::delete('/delete/{category}', 'CategoryController@delete')->name('delete');
                    });
                });

                Route::prefix('products')->group(function () {
                    Route::name('products.')->group(function () {
                        Route::get('/', 'ProductController@index')->name('index');
                        Route::get('/store', 'ProductController@add')->name('add');
                        Route::post('/store', 'ProductController@store')->name('store');
                        Route::get('/detail/{product}', 'ProductController@detail')->name('edit');
                        Route::put('/update/{product}', 'ProductController@update')->name('update');
                        Route::delete('/delete/{product}', 'ProductController@delete')->name('delete');
                        Route::get('/change_status/{product}', 'ProductController@changeStatus')->name('change.status');
                    });
                });

                Route::prefix('projects_under_construction')->group(function () {
                    Route::name('projects_under_construction.')->group(function () {
                        Route::get('/', 'ProjectUnderConstructionController@index')->name('index');
                        Route::get('/store', 'ProjectUnderConstructionController@add')->name('add');
                        Route::post('/store', 'ProjectUnderConstructionController@store')->name('store');
                        Route::get('/detail/{project_building}', 'ProjectUnderConstructionController@detail')->name('edit');
                        Route::put('/update/{project_building}', 'ProjectUnderConstructionController@update')->name('update');
                        Route::delete('/delete/{project_building}', 'ProjectUnderConstructionController@delete')->name('delete');
                    });
                });

                Route::prefix('knowledges')->group(function () {
                    Route::name('knowledges.')->group(function () {
                        Route::get('/', 'KnowledgeController@index')->name('index');
                        Route::get('/store', 'KnowledgeController@add')->name('add');
                        Route::post('/store', 'KnowledgeController@store')->name('store');
                        Route::get('/detail/{knowledge}', 'KnowledgeController@detail')->name('edit');
                        Route::put('/update/{knowledge}', 'KnowledgeController@update')->name('update');
                        Route::delete('/delete/{knowledge}', 'KnowledgeController@delete')->name('delete');
                    });
                });

                Route::prefix('news')->group(function () {
                    Route::name('news.')->group(function () {
                        Route::get('/', 'NewsController@index')->name('index');
                        Route::get('/store', 'NewsController@add')->name('add');
                        Route::post('/store', 'NewsController@store')->name('store');
                        Route::get('/detail/{newspaper}', 'NewsController@detail')->name('edit');
                        Route::put('/update/{newspaper}', 'NewsController@update')->name('update');
                        Route::delete('/delete/{newspaper}', 'NewsController@delete')->name('delete');
                    });
                });

                Route::prefix('tags')->group(function () {
                    Route::name('tags.')->group(function () {
                        Route::get('/', 'TagController@index')->name('index');
                        Route::get('/store', 'TagController@add')->name('add');
                        Route::post('/store', 'TagController@store')->name('store');
                        Route::get('/detail/{tag}', 'TagController@detail')->name('edit');
                        Route::put('/update/{tag}', 'TagController@update')->name('update');
                        Route::delete('/delete/{tag}', 'TagController@delete')->name('delete');
                    });
                });

                Route::prefix('reviews')->group(function () {
                    Route::name('reviews.')->group(function () {
                        Route::get('/{rating_product?}', 'ReviewController@index')->name('index');
                        Route::get('/detail/{review}', 'ReviewController@detail')->name('edit');
                        Route::delete('/delete/{review}', 'ReviewController@delete')->name('delete');
                        Route::get('/change_status/{review}', 'ReviewController@changeStatus')->name('change.status');
                    });
                });

                Route::prefix('feedbacks')->group(function () {
                    Route::name('feedbacks.')->group(function () {
                        Route::get('/{news_id?}', 'FeedbackController@index')->name('index');
                        Route::delete('/delete/{feedback}', 'FeedbackController@delete')->name('delete');
                    });
                });
                
            });
        });
    });
});

Route::get('/home', 'HomeController@index')->name('home');

Route::name('public.')->group( function () {
    Route::get('/', 'IndexController@index')->name('home.index');
    Route::get('/gioi-thieu', 'IndexController@introduction')->name('introduction');
    Route::get('/chinh-sach-quy-dinh', 'IndexController@policyRegulation')->name('policy.regulation');
    Route::get('/danh-muc/{category}', 'CategoryController@detail')->name('category');
    Route::get('/san-pham/{product}', 'ProductController@detail')->name('product');
    Route::post('/review', 'ReviewController@store')->name('review.product');
    Route::get('/kien-thuc/{knowledge}', 'KnowledgeController@detail')->name('knowledge');
    Route::get('/kien-thuc/tin-tuc/{newspaper}', 'NewsController@detail')->name('news.detail');
    Route::post('/feedback', 'FeedbackController@store')->name('feedback');
    Route::get('/tags/{tag}', 'TagController@detail')->name('tags');
    Route::get('/du-an-dang-thi-cong', 'ProjectUnderConstructionController@index')->name('project.building.index');
    Route::get('/du-an-dang-thi-cong/{project_building}', 'ProjectUnderConstructionController@detail')->name('project_building.detail');
});
