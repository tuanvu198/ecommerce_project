<?php

return [
    'facebook' => '<li><a href=":url" class="social-button :class" id=":id"><span class="icon-social-facebook icons"></span></a></li>',
    'twitter' => '<li><a href=":url" class="social-button :class" id=":id"><span class="icon-social-twitter icons"></span></a></li>',
    'gplus' => '<li><a href=":url" class="social-button :class" id=":id"><span class="icon-social-google icons"></span></a></li>',
    'linkedin' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fa fa-linkedin"></span></a></li>',
    'whatsapp' => '<li><a target="_blank" href=":url" class="social-button :class" id=":id"><span class="fa fa-whatsapp"></span></a></li>',
    'pinterest' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fa fa-pinterest"></span></a></li>',
];
