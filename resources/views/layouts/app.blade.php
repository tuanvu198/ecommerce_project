<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Công ty cổ phần công nghệ LEDONE Việt Nam') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- This site is optimized with the Yoast SEO plugin v11.7 - https://yoast.com/wordpress/plugins/seo/ -->
    <meta name="description" content="LEDONE cung cấp các loại màn hình led trong nhà, ngoài trời, màn hình ghép, màn hình lcd quảng cáo, led fullcolor cao cấp. Thi công màn hình led Uy Tín, Nhanh Chóng, Chuyên Nghiệp"/>
    <link rel="canonical" href="https://manhinhledfullcolor.com/" />
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Công ty cổ phần công nghệ LEDONE Việt Nam" />
    <meta property="og:description" content="LEDONE cung cấp các loại màn hình led trong nhà, ngoài trời, màn hình ghép, màn hình lcd quảng cáo, led fullcolor cao cấp. Thi công màn hình led Uy Tín, Nhanh Chóng, Chuyên Nghiệp" />
    <meta property="og:url" content="https://manhinhledfullcolor.com/" />
    <meta property="og:site_name" content="Công ty cổ phần công nghệ LEDONE Việt Nam" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="LEDONE cung cấp các loại màn hình led trong nhà, ngoài trời, màn hình ghép, màn hình lcd quảng cáo, led fullcolor cao cấp. Thi công màn hình led Uy Tín, Nhanh Chóng, Chuyên Nghiệp" />
    <meta name="twitter:title" content="Công ty cổ phần công nghệ LEDONE Việt Nam" />
    <meta name="twitter:site" content="@quangcaohiendai" />
    <meta name="twitter:creator" content="@quangcaohiendai" />
    <script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://manhinhledfullcolor.com/#organization","name":"C\u00f4ng ty c\u1ed5 ph\u1ea7n c\u00f4ng ngh\u1ec7 ledone vi\u1ec7t nam","url":"https://manhinhledfullcolor.com/","sameAs":["https://www.facebook.com/manhinhledvietnam/","https://www.instagram.com/ledonevietnam","https://myspace.com/ledonevietnam","https://www.pinterest.com/ledonevietnam","https://twitter.com/quangcaohiendai"],"logo":{"@type":"ImageObject","@id":"https://manhinhledfullcolor.com/#logo","url":"https://manhinhledfullcolor.com/wp-content/uploads/2018/05/logofullcolor-1.png","width":400,"height":84,"caption":"C\u00f4ng ty c\u1ed5 ph\u1ea7n c\u00f4ng ngh\u1ec7 ledone vi\u1ec7t nam"},"image":{"@id":"https://manhinhledfullcolor.com/#logo"}},{"@type":"WebSite","@id":"https://manhinhledfullcolor.com/#website","url":"https://manhinhledfullcolor.com/","name":"C\u00f4ng ty c\u1ed5 ph\u1ea7n c\u00f4ng ngh\u1ec7 LEDONE Vi\u1ec7t Nam","publisher":{"@id":"https://manhinhledfullcolor.com/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://manhinhledfullcolor.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://manhinhledfullcolor.com/#webpage","url":"https://manhinhledfullcolor.com/","inLanguage":"vi-VN","name":"C\u00f4ng ty c\u1ed5 ph\u1ea7n c\u00f4ng ngh\u1ec7 LEDONE Vi\u1ec7t Nam","isPartOf":{"@id":"https://manhinhledfullcolor.com/#website"},"about":{"@id":"https://manhinhledfullcolor.com/#organization"},"datePublished":"2017-06-23T07:54:20+00:00","dateModified":"2019-06-06T09:57:14+00:00","description":"LEDONE cung c\u1ea5p c\u00e1c lo\u1ea1i m\u00e0n h\u00ecnh led trong nh\u00e0, ngo\u00e0i tr\u1eddi, m\u00e0n h\u00ecnh gh\u00e9p, m\u00e0n h\u00ecnh lcd qu\u1ea3ng c\u00e1o, led fullcolor cao c\u1ea5p. Thi c\u00f4ng m\u00e0n h\u00ecnh led Uy T\u00edn, Nhanh Ch\u00f3ng, Chuy\u00ean Nghi\u1ec7p"}]}</script>
    <!-- / Yoast SEO plugin. -->


    <!--Start of Tawk.to Script (0.3.3)-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{};
        var Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5b343a7fd0b5a54796823e97/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script (0.3.3)-->
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Welcome') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
