@extends('frontend.layouts.app')

@section('title',"Dự án thi công | Công ty cổ phần công nghệ LEDONE Việt Nam")

@section('content')
    <div class="container" style="margin-top: 20px;">
        <div class="row">
            @forelse($projectsUnderConstruction as $project)
                <div class="media table-hover">
                    <div class="media-left" style="width:20%">
                        <a href="{{ route('public.project_building.detail', ['slug' => $project->slug]) }}"><img src="{{asset('storage/projects_under_construction/'. $project->image) }}" class="{{ $project->name }}"></a>
                    </div>
                    <div class="media-body">
                        <h2 class="media-heading"><a href="{{ route('public.project_building.detail', ['slug' => $project->slug]) }}">{{ $project->name }}</a></h2>
                        <p>{{ $project->summary }}</p>
                    </div>
                    <div class="bl__date">
                        <span>{{ date_format(new DateTime($project->updated_at), 'd-m-Y') }}</span>
                    </div>
                </div>
                <hr>
            @empty
                <p>@lang('messages.no_data')</p>
            @endforelse
        </div>
    </div>
@endsection