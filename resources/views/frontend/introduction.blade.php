@extends('frontend.layouts.app')

@section('title', 'Giới thiệu')


@section('content')
    <section class="htc__category__area ptb--100">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section__title--2 text-center">
                        <h3 class="title__line" style="font-style: italic; font-weight: bold">Công ty cổ phần công nghệ
                            LEDONE Việt Nam đơn vị cung cấp, cho thuê giải
                            pháp màn hình LED, màn hình ghép, màn hình LCD quảng cáo nhập khẩu hàng đầu Việt Nam. Dưới
                            đây sẽ là thông tin chi tiết về chúng tôi.</h3>
                    </div>
                    <div class="about-us">
                        <h3 style="padding-bottom: 15px;"><strong>Tên công ty:</strong> Công Ty Cổ Phần Công Nghệ LEDONE
                            Việt Nam</h3>
                        <h3 style="padding-bottom: 15px;"><strong>Tên giao dịch quốc tế:</strong> Ledone Viet Nam
                            Technology Joint Stock Company</h3>
                        <h3 style="padding-bottom: 15px;"><strong>Mã số thuế: </strong> 0108054730</h3>
                        <h3 style="padding-bottom: 15px;"><strong>Giấy phép kinh doanh: </strong> 0108054730 – ngày cấp:
                            10/11/2017</h3>
                        <h3 style="padding-bottom: 15px;"><strong>Ngày hoạt động: </strong> 09/11/2017</h3>
                        <h3 style="padding-bottom: 15px;"><strong>Giám đốc:</strong> Nguyễn Thị Quyên</h3>
                        <h3 style="padding-bottom: 15px;"><strong>Điện thoại:</strong> 0973 992 887</h3>
                        <h3 style="padding-bottom: 15px;"><strong>Trụ sở:</strong> Số 104, ngõ 136 Đê La Thành, Phường
                            Trung Phụng, Quận Đống Đa, Thành phố Hà Nội</h3>
                        <h3 style="padding-bottom: 15px;"><strong>Chi nhánh Hà Nội:</strong> Số 104, Ngõ 360, Xã Đàn,
                            Đống Đa, Hà Nội</h3>
                        <h3 style="padding-bottom: 15px;"><strong>Chi nhánh miền Trung:</strong> Số 112 Bế văn Đàn,
                            Phường Chính Gián, Quận Thanh Khê, TP. Đà Nẵng</h3>
                        <h3 style="padding-bottom: 15px;"><strong>Chi nhánh miền Nam:</strong> Số 1/24 Nguyễn Văn Dung,
                            Phường 6, Quận Gò Vấp. TP.HCM</h3>
                        <h3 style="padding-bottom: 15px;"><strong>Quy mô công ty:</strong> S60 nhân viên.</h3>
                        <h3 style="padding-top: 50px;">
                            <img src="{{ asset('frontend/images/gioi-thieu-ledone-viet-nam.jpg') }}"
                                 class="img-rounded center" alt="Công ty cổ phần công nghệ LEDONE Việt Nam">
                        </h3>
                        <p style="text-align: center;"><em>Công ty cổ phần công nghệ LEDONE Việt Nam</em></p>
                        <h2 style="padding-top: 50px; padding-bottom: 20px;">LĨNH VỰC HOẠT ĐỘNG CỦA CÔNG TY CỔ PHẦN CÔNG
                            NGHỆ LEDONE VIỆT NAM</h2>
                        <p style="padding-bottom: 15px;">+ Cung cấp, cho thuê, thiết kế, lắp đặt màn hình LED trong nhà
                            (indoor), ngoài trời (Outdoor)</p>
                        <p style="padding-bottom: 15px">+ Cung cấp, cho thuê, thiết kế, lắp đặt màn hình ghép Videowall
                            của các hãng nổi tiếng: LG, Samsung, Panasonic, Toshiba</p>
                        <p style="padding-bottom: 15px">+ Cung cấp, lắp đặt màn hình quảng cáo Frame sảnh chờ, trong
                            thang máy</p>
                        <p style="padding-bottom: 15px">+ Cung cấp, cho thuê, lắp đặt màn hình LCD quảng cáo cảm ứng
                            chân đứng, treo tường, chân quỳ Androi/Window</p>
                        <p style="padding-bottom: 15px">+ Trang trí đường phố, nội thất quán Bar, Karaoke, nhà hàng,
                            khách sạn bằng công nghệ LED</p>
                        <p style="padding-bottom: 15px">+ Cung cấp linh kiện, vật tư ngành LED</p>
                        <p style="padding-bottom: 15px">+ Cung cấp phần mềm quản lý từ xa cho các thiết bị màn hình LED,
                            màn hình ghép, màn hình LCD quảng cáo</p>
                        <p style="padding-bottom: 15px">
                            <img src="{{ asset('frontend/images/gioi-thieu-ledone-viet-nam-1-768x356.jpg') }}"
                                 class="img-rounded center" alt="Công ty cổ phần công nghệ LEDONE Việt Nam">
                        </p>
                        <p style="text-align: center;"><em>Lĩnh vực hoạt động, kinh doanh của doanh nghiệp</em></p>
                        <h2 style="padding-top: 50px; padding-bottom: 20px;">NGUỒN GỐC SẢN PHẨM</h2>
                        <p style="padding-bottom: 15px">Những linh kiện màn hình của chúng tôi được cung cấp bởi các nhà
                            sản xuất uy tín hàng đầu Thế giới đến từ Mỹ, Nhật Bản, Hàn Quốc, Đài Loan, Trung Quốc… Và
                            được lắp ráp trực tiếp tại Shenzhen, Trung Quốc – Khu công xưởng lớn nhất Thế Giới . Mỗi sản
                            phẩm đều là sự kết hợp sáng tạo và đột phá về công nghệ, không bị giới hạn về kiểu dáng,
                            kích cỡ, tính năng, phần mềm, không gian sử dụng… đáp ứng tối đa nhu cầu sử dụng của khách
                            hàng.</p>
                        <p style="text-align: center;">
                            <iframe src="https://www.youtube.com/embed/EsXM2JBmaa0" width="560" height="315"
                                    frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                        </p>
                        <p style="text-align: center;"><em>Xưởng lắp ráp màn hình LED tại Thâm Quyến – Trung Quốc</em>
                        </p>
                        <h2 style="padding-top: 50px; padding-bottom: 20px;">SỨ MỆNH</h2>
                        <p style="padding-bottom: 15px">LEDONE luôn luôn nỗ lực cải tiến chất lượng dịch vụ cũng như sản
                            phẩm để mang lại những trải nghiệm tốt nhất cho khách hàng ở bất cứ nơi đâu, góp phần thúc
                            đẩy kinh tế – xã hội của Việt Nam</p>
                        <h2 style="padding-top: 50px; padding-bottom: 20px;">TẦM NHÌN</h2>
                        <p style="padding-bottom: 15px">Luôn đón đầu công nghệ, mong muốn cung cấp đến cho khách hàng
                            những sản phẩm chất, hiện đại nhất với giá thành cạnh tranh nhất. LEDONE luôn mang đến cho
                            khách hàng những trải nghiệm sản phẩm và dịch vụ tốt nhất cho dù bạn ở bất cứ nơi đâu.</p>
                        <p style="padding-bottom: 15px">
                            <img src="{{ asset('frontend/images/gioi-thieu-ledone-viet-nam-2.jpg') }}"
                                 class="img-rounded center" alt="Công ty cổ phần công nghệ LEDONE Việt Nam">
                        </p>
                        <p style="text-align: center; padding-bottom: 15px"><em>Sứ mạnh và tầm nhùn của LEDONE là đem
                                lại những sản phẩm chất lượng cao cho khách hàng, góp phần tạo lập thương hiệu uy tín số
                                1 tại việt Nam</em></p>
                        <h2 style="text-align: justify; padding-bottom: 15px">TRIẾT LÝ KINH DOANH: “CHẤT LƯỢNG CAO HƠN
                            DOANH SỐ”</h2>
                        <p style="text-align: justify; padding-bottom: 15px">Là một trong những doanh nghiệp hoạt động
                            uy tín và chuyên nghiệp hàng đầu trong lĩnh vực cung cấp thiết bị trình chiếu hiện đại.
                            Những yếu tố quyết định tới thành công của doanh nghiệp không thể thiếu đó là chất lượng
                            dịch vụ – sản phẩm. </p>
                        <p style="text-align: justify; padding-bottom: 15px">LEDONE với triết lý kinh doanh “Chất lượng
                            cao hơn doanh số”, chúng tôi luôn hoàn thiện quy trình quản lý chất lượng của mình từ khâu
                            sản xuất, kinh doanh, chăm sóc khách hàng, bảo hành, bảo trì và những chế độ ưu đãi đặc biệt
                            với mong muốn đáp ứng nhu cầu của khách hàng một cách tối ưu nhất.</p>
                        <h2 style="text-align: justify; padding-bottom: 15px">GIÁ TRỊ NHÂN VĂN</h2>
                        <p style="text-align: justify; padding-bottom: 15px">Ngay từ những ngày đầu thành lập LEDONE đã
                            hướng tới phục vụ khách hàng một cách tốt nhất, nâng cao tinh thần – giá trị của nhân viên
                            trong doanh nghiệp, mang lại lợi ích cho đối tác, đóng góp vì lợi ích chung của cộng
                            đồng.</p>
                        <h2 style="text-align: justify; padding-bottom: 15px">ĐỐI TÁC THÂN THIẾT</h2>
                        <p style="text-align: justify; padding-bottom: 15px">Chúng tôi là đối tác thân thiết của những
                            doanh nghiệp, tập đoàn lớn như: Viettel, NEM, SunGroup, Unitel, sân bay quốc tế Nội Bài, Đà
                            nẵng, Tân Sơn Nhất….</p>
                        <p style="padding-bottom: 15px">
                            <img src="{{ asset('frontend/images/gioi-thieu-ledone-viet-nam-3.jpg') }}"
                                 class="img-rounded center" alt="Công ty cổ phần công nghệ LEDONE Việt Nam">
                        </p>
                        <p style="text-align: center;"><em>Hoạt động theo phương châm “chất lượng cao hơn doanh số” là
                                đối tác lớn của nhiều doanh nghiệp lớn tại Việt Nam</em></p>
                        <h2 style="text-align: justify; padding-bottom: 15px">THÔNG TIN LIÊN HỆ</h2>
                        <p style="padding-bottom: 15px">Mọi thắc mắc, quý khách có thể liên hệ với chúng tôi theo địa
                            chỉ</p>
                        <div class="div-tu-van"
                             style="padding: 5px 10px; line-height: 22px; margin: 10px 0px; background: none 0% 0% repeat scroll #eeeeee; border: 1px dashed #81b855; text-align: justify;">
                            <p style="text-align: justify;"><span
                                        style="font-family: 'Roboto Condensed', sans-serif;"><strong>Công ty cổ phần Công nghệ LEDONE Việt Nam:</strong></span>
                            </p>
                            <p style="text-align: justify;"><span
                                        style="font-family: 'Roboto Condensed', sans-serif;"><strong><img
                                                class="alignnone wp-image-6761"
                                                src="{{asset('frontend/images/trụ-sở-9-400x400.png')}}"
                                                alt="trụ sở 9" width="17" height="17"
                                                sizes="(max-width: 17px) 100vw, 17px">&nbsp;</strong><span
                                            style="font-size: 90%;">Trụ sở: 104 Ngõ 360 Xã Đàn, P. Trung Phụng, Q. Đống Đa, TP. Hà Nội</span></span>
                            </p>
                            <p style="text-align: justify;"><span
                                        style="font-family: 'Roboto Condensed', sans-serif;"><img
                                            class="alignnone wp-image-6751"
                                            src="{{asset('frontend/images/phone-icon.png')}}"
                                            alt="liên hệ" width="17" height="17"
                                            sizes="(max-width: 17px) 100vw, 17px">&nbsp;<span style="font-size: 90%;">Hotline: &nbsp;0938.5555.36 ( Ms. Quyên ) – 097 999 2025 ( Mr. Lực )</span></span>
                            </p>
                            <p style="text-align: justify;"><strong><img class="alignnone wp-image-6763"
                                                                         src="{{ asset('frontend/images/icon-website-400x400.png') }}"
                                                                         alt="icon website" width="17" height="17"
                                                                         sizes="(max-width: 17px) 100vw, 17px"><span
                                            style="font-size: 90%;">&nbsp;</span></strong><span style="font-size: 90%;">Website: <span
                                            style="color: #333399;"><strong><a href="https://manhinhledfullcolor.com">manhinhledfullcolor.com</a></strong></span></span>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
