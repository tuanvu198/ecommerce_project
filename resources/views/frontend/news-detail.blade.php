@extends('frontend.layouts.app')

@section('title',"$news->name | Công ty cổ phần công nghệ LEDONE Việt Nam")

@section('content')
    <div class="ht__bradcaump__area"
         style="background: rgba(0, 0, 0, 0) url('{{ asset('storage/news/'. $news->image) }}') no-repeat scroll center center / cover ;">
        <div class="ht__bradcaump__wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="bradcaump__inner">
                            <nav class="bradcaump-inner">
                                <a class="breadcrumb-item" href="{{ route('public.home.index') }}">Trang chủ</a>
                                <span class="brd-separetor"><i class="zmdi zmdi-chevron-right"></i></span>
                                <span class="breadcrumb-item active">{{ $news->name }}</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="htc__blog__details bg__white ptb--100">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-12">
                    <div class="htc__blog__details__wrap">
                        <div class="ht__bl__thumb">
                            <img src="{{ asset('storage/news/'. $news->image) }}" alt="{{ $news->name }}">
                        </div>
                        <div class="bl__dtl">
                            {!! $news->desc !!}
                        </div>
                        <div class="sin__desc product__share__link">
                            <p><span>Chia sẻ:</span></p>
                            {!!
                                Share::currentPage(null, ['class' => 'my-class', 'id' => 'my-id'])
                                        ->facebook()
                                        ->twitter()
                                        ->googlePlus()
                            !!}
                        </div>
                        <!-- Start comment Form -->
                        <div class="ht__comment__form">
                            <form action="{{ route('public.feedback') }}" method="post">
                                @csrf
                                <h4 class="title__line--5">Phản hồi</h4>
                                <div class="ht__comment__form__inner">
                                    <div class="comment__form">
                                        <input type="text" name="name" value="{{ old('name') }}" placeholder="Name *">
                                        <input type="email" name="email" value="{{ old('email') }}" placeholder="Email *">
                                        <input type="text" value="{{ old('website_address') }}"  name="website_address" placeholder="Website">
                                        <input type="hidden" name="news_id"  value="{{ $news->id }}" placeholder="Website">
                                    </div>
                                    <div class="comment__form message">
                                        <textarea name="content" placeholder="Your Comment *">{{ old('website_address') }}</textarea>
                                    </div>
                                </div>
                                <div class="ht__comment__btn--2 mt--30">
                                    <button type="submit" class="fr__btn" href="#">Gửi</button>
                                </div>
                            </form>
                        </div>
                        <!-- End comment Form -->
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection