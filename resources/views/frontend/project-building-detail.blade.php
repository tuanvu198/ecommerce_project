@extends('frontend.layouts.app')

@section('title',"$projectUnderConstruction->name")

@section('content')
<div class="container">
    <div class="ht__bradcaump__area"
    style="background: rgba(0, 0, 0, 0) url('{{ asset('storage/projects_under_construction/'. $projectUnderConstruction->image) }}') no-repeat scroll center center / cover ;">
   <div class="ht__bradcaump__wrap">
       <div class="container">
           <div class="row">
               <div class="col-xs-12">
                   <div class="bradcaump__inner">
                       <nav class="bradcaump-inner">
                           <a class="breadcrumb-item" href="{{ route('public.home.index') }}">Trang chủ</a>
                           <span class="brd-separetor"><i class="zmdi zmdi-chevron-right"></i></span>
                           <span class="breadcrumb-item active">{{ $projectUnderConstruction->name }}</span>
                       </nav>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
<section class="htc__blog__details bg__white ptb--100">
   <div class="container">
       <div class="row">
           <div class="col-xs-12 col-lg-12">
               <div class="htc__blog__details__wrap">
                   <div class="ht__bl__thumb">
                       <img src="{{ asset('storage/projects_under_construction/'. $projectUnderConstruction->image) }}" alt="{{ $projectUnderConstruction->name }}">
                   </div>
                   <div class="bl__dtl">
                       {!! $projectUnderConstruction->desc !!}
                   </div>
               </div>
           </div>
       </div>
   </div>
   <div class="sin__desc product__share__link" style="margin-left: 1%;">
       <p><span>Chia sẻ:</span></p>
       {!!
           Share::currentPage(null, ['class' => 'my-class', 'id' => 'my-id'])
                   ->facebook()
                   ->twitter()
                   ->googlePlus()
       !!}
   </div>
</section>
</div>
   

@endsection