@extends('frontend.layouts.app')

@section('title',"$product->name")

@section('content')
    <div class="ht__bradcaump__area"
         style="background: rgba(0, 0, 0, 0) url('{{ asset('storage/products/'. $product->image) }}') no-repeat scroll center center / cover ;">
        <div class="ht__bradcaump__wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="bradcaump__inner">
                            <nav class="bradcaump-inner">
                                <a class="breadcrumb-item" href="{{ route('public.home.index') }}}">Trang chủ</a>
                                <span class="brd-separetor"><i class="zmdi zmdi-chevron-right"></i></span>
                                <a class="breadcrumb-item"
                                   href="{{ route('public.category',['slug' => $product->category->slug ]) }}">{{ $product->category->name }}</a>
                                <span class="brd-separetor"><i class="zmdi zmdi-chevron-right"></i></span>
                                <span class="breadcrumb-item active">{{ $product->name }}</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Start Product Details Area -->
    <section class="htc__product__details bg__white ptb--100">
        <!-- Start Product Details Top -->
        <div class="htc__product__details__top">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
                        <div class="htc__product__details__tab__content">
                            <!-- Start Product Big Images -->
                            <div class="product__big__images">
                                <div class="portfolio-full-image tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="img-tab-1">
                                        <img src="{{ asset('storage/products/'. $product->image) }}"
                                             alt="{{ $product->name }}">
                                    </div>
                                </div>
                            </div>
                            <!-- End Product Big Images -->
                            <!-- Start Small images -->
                            <!-- End Small images -->
                        </div>
                    </div>
                    <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12 smt-40 xmt-40">
                        <div class="ht__product__dtl">
                            <h2>{{ $product->name }}</h2>
                            <ul class="rating">
                                <li><i class="icon-star icons"></i></li>
                                <li><i class="icon-star icons"></i></li>
                                <li><i class="icon-star icons"></i></li>
                                <li><i class="icon-star icons"></i></li>
                                <li><i class="icon-star icons"></i></li>
                            </ul>
                            <ul class="pro__prize">
                                @if($product->promotion == App\Model\Product::PROMOTION)
                                    <li class="old__prize">{{ number_format($product->price, 0 , ',', '.' )}} vnđ</li>
                                @endif
                                <li>{{ number_format($product->price - $product->sale_off, 0 , ',', '.')}} vnđ</li>
                            </ul>
                            <p class="pro__info">{{ $product->summary }}</p>
                            <div class="ht__pro__desc">
                                <div class="sin__desc">
                                    <p><span>Khả dụng:</span> Sản phẩm đang được bán</p>
                                </div>
                                <div class="sin__desc align--left">
                                    <p><span>Danh mục các sản phẩm liên quan:</span></p>
                                    <ul class="pro__cat__list">
                                        @forelse($categories as $ca)
                                            <li>
                                                <a href="{{ route('public.category', ['slug' => $ca->slug]) }}">{{ $ca->name }}</a>
                                            </li>
                                        @empty
                                            <li><a href="#">@lang('messages.no_data')</a></li>
                                        @endforelse
                                    </ul>
                                </div>
                                <div class="sin__desc align--left">
                                    <p><span>Tags:</span></p>
                                    <ul class="pro__cat__list">
                                        <ul class="pro__cat__list">
                                            @forelse($tags as $tag)
                                                <li>
                                                    <a href="{{ route('public.tags', ['slug' => $tag->slug]) }}">{{ $tag->name }}
                                                        ,</a></li>
                                            @empty
                                                <li><a href="#">@lang('messages.no_data')</a></li>
                                            @endforelse
                                        </ul>
                                    </ul>
                                </div>

                                <div class="sin__desc product__share__link">
                                    <p><span>Chia sẻ:</span></p>
                                    {!!
                                        Share::currentPage(null, ['class' => 'my-class', 'id' => 'my-id'])
                                                ->facebook()
                                                ->twitter()
                                                ->googlePlus()
                                    !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Product Details Top -->
    </section>
    <!-- End Product Details Area -->
    <!-- Start Product Description -->
    <section class="htc__produc__decription bg__white">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Start List And Grid View -->
                    <ul class="pro__details__tab" role="tablist">
                        <li role="presentation" class="description active"><a href="#description" role="tab"
                                                                              data-toggle="tab">Miêu tả</a></li>
                        <li role="presentation" class="review"><a href="#review" role="tab" data-toggle="tab">Đánh
                                giá</a>
                        </li>
                    </ul>
                    <!-- End List And Grid View -->
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="ht__pro__details__content">
                        <!-- Start Single Content -->
                        <div role="tabpanel" id="description" class="pro__single__content tab-pane fade in active">
                            <div class="pro__tab__content__inner">
                                {!! $product->desc !!}
                            </div>
                        </div>
                        <!-- End Single Content -->
                        <!-- Start Single Content -->
                        <div role="tabpanel" id="review" class="pro__single__content tab-pane fade">
                            <div class="pro__tab__content__inner">
                                <div class="card">
                                    <div class="card-body">
                                        <!-- Right-aligned -->
                                        @forelse($product->reviews as $review)
                                            @if($review->status == \App\Model\Review::PUBLIC)
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <img src="{{ asset('frontend/images/default-review.jpg') }}"
                                                             class="img img-rounded img-fluid"/>
                                                        <p class="text-secondary text-center">{{ date_format(new DateTime($review->updated_at), 'd-m-Y') }}</p>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <p>
                                                            <a class="float-left"
                                                               href="#"><strong></strong></a>
                                                            <span class="float-right"><i
                                                                        class="text-warning fa fa-star"></i></span>
                                                            <span class="float-right"><i
                                                                        class="text-warning fa fa-star"></i></span>
                                                            <span class="float-right"><i
                                                                        class="text-warning fa fa-star"></i></span>
                                                            <span class="float-right"><i
                                                                        class="text-warning fa fa-star"></i></span>
                                                        </p>
                                                        <div class="clearfix"></div>
                                                        <p>{{ $review->content }}</p>
                                                    </div>
                                                </div>
                                            @endif
                                        @empty
                                            <div class="row">
                                                <p class="text-center">@lang('messages.no_data')</p>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                <div class="row" style="margin-top:40px;">
                                    <div class="col-md-12">
                                        <div class="well well-sm">
                                            <div class="text-right">
                                                <a class="btn btn-success btn-green" href="javascript::void(0)"
                                                   id="open-review-box">Đánh giá</a>
                                            </div>
                                            <div class="row" id="post-review-box" style="display:none;">
                                                <div class="col-md-12">
                                                    @include('frontend/component/alert')
                                                    <form accept-charset="UTF-8" action="{{ route('public.review.product') }}" method="post">
                                                        @csrf
                                                        <input id="ratings-hidden" value="" name="rating" type="hidden">
                                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                                        <textarea class="form-control animated" cols="50"
                                                                  id="new-review" name="content"
                                                                  placeholder="Viết đánh giá tại đây ..."
                                                                  rows="5"></textarea>
                                                        <div class="form-row" style="padding-top: 20px;">
                                                            <div class="form-group col-md-6">
                                                                <label for="inputEmail4">Email</label>
                                                                <input type="email" name="email" class="form-control"
                                                                       placeholder="Email">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="inputPassword4">Tên</label>
                                                                <input type="text" name="name" class="form-control"
                                                                       placeholder="Tên">
                                                            </div>
                                                        </div>
                                                        <div class="text-right">
                                                            <div class="stars starrr" data-rating="0"></div>
                                                            <a class="btn btn-danger btn-sm" href="#"
                                                               id="close-review-box"
                                                               style="display:none; margin-right: 10px;">
                                                                <span class="glyphicon glyphicon-remove"></span>Hủy</a>
                                                            <button class="btn btn-success btn-lg" type="submit">Lưu
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Content -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Product Description -->
    <!-- Start Product Area -->
    <section class="htc__product__area--2 pb--100 product-details-res">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section__title--2 text-center">
                        <h2 class="title__line">Sản phẩm liên quan</h2>
                        <p>danh sách sản phẩm liên quan</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="product__wrap clearfix">
                    <!-- Start Single Product -->
                    @forelse($list_products as $product_relation)
                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                            <div class="category">
                                <div class="ht__cat__thumb">
                                    <a href="{{ route('public.product', ['slug' => $product_relation->slug]) }}">
                                        <img src="{{ asset('storage/products/'. $product_relation->image) }}" alt="{{ $product_relation->name }}">
                                    </a>
                                </div>
                                <div class="fr__product__inner">
                                    <h4><a href="{{ route('public.product', ['slug' => $product_relation->slug]) }}">{{ $product_relation->name }}</a></h4>
                                    <ul class="fr__pro__prize">
                                        @if($product_relation->promotion == App\Model\Product::PROMOTION)
                                        <li class="old__prize">{{ number_format($product->price, 0 , ',', '.' )}} vnđ</li>
                                        @endif
                                        <li>{{ number_format($product->price - $product->sale_off, 0 , ',', '.')}} vnđ</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Product -->
                    @empty
                        <p>@lang('messages.no_data')</p>
                    @endforelse
                </div>
            </div>
        </div>
    </section>
    <!-- End Product Area -->

@endsection