@extends('frontend.layouts.app')

@section('title', 'Công ty cổ phần LEDONE Việt Nam')


@section('content')

    <!-- Start Slider Area -->
    <div class="slider__container slider--one bg__cat--3">
        <div class="slide__container slider__activation__wrap owl-carousel">
            <!-- Start Single Slide -->
            @foreach($categories as $category)
                <div class="single__slide animation__style01 slider__fixed--height">
                    <div class="container">
                        <div class="row align-items__center">
                            <div class="col-md-7 col-sm-7 col-xs-12 col-lg-6">
                                <div class="slide">
                                    <div class="slider__inner">
                                        <h2>các dòng sản phẩm</h2>
                                        <h1 style="font-size:30px;">{{ strtoupper($category->name) }}</h1>
                                        <div class="cr__btn">
                                            <a href="{{ route('public.category', ['slug' => $category->slug ]) }}">Xem
                                                ngay</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-5 col-xs-12 col-md-5">
                                <div class="slide__thumb">
                                    <img src="{{asset('storage/categories/'. $category->image)}}"
                                         alt="{{ $category->name }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        @endforeach
        <!-- End Single Slide -->
        </div>
    </div>
    <!-- Start Slider Area -->
    <!-- Start Category Area -->
    <section class="htc__category__area ptb--100">
        <div class="container">
            @forelse($categories as $category)
                @if(count($category->products))
                    <div class="row" style="margin-top: 30px;">
                        <div class="col-xs-12">
                            <div class="section__title--2 text-center">
                                <h2 class="title__line"><span><i class="fas fa-star"></i></span> &nbsp;
                                    &nbsp;{{ strtoupper($category->name) }}</h2>
                                <p>sản phẩm cùng thể loại</p>
                            </div>
                        </div>
                    </div>
                    <div class="htc__product__container">
                        <div class="row">
                            <div class="product__list clearfix mt--30">
                            @foreach($category->products as $product)
                                <!-- Start Single Category -->
                                    <div class="col-md-4 col-lg-3 col-sm-4 col-xs-12">
                                        <div class="category">
                                            <div class="ht__cat__thumb">
                                                <a href="{{ route('public.product', ['slug' => $product->slug ]) }}">
                                                    <img src="{{asset('storage/products/'. $product->image)}}"
                                                         alt="product images">
                                                </a>
                                            </div>
                                            <div class="fr__product__inner">
                                                <h4>
                                                    <a href="{{ route('public.product', ['slug' => $product->slug ]) }}">{{ $product->name }}</a>
                                                </h4>
                                                <ul class="fr__pro__prize">
                                                    @if($product->promotion == \App\Model\Product::PROMOTION)
                                                        <li class="old__prize">{{ number_format($product->price, 0 , ',', '.') }} vnđ</li>
                                                    @endif
                                                    <li>{{ number_format($product->price - $product->sale_off, 0 , ',', '.') }}vnđ
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- End Single Category -->
                        <!-- End Single Category -->
                    </div>
                @endif
            @empty
            @endforelse

        </div>
    </section>
    <!-- End Category Area -->
    <!-- Start Prize Good Area -->
    <section class="htc__good__sale bg__cat--3">
        <div class="container">
            <div class="row">
                @if(!empty($knowledge))
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                        <div class="fr__prize__inner">
                            <h2>{{ $knowledge->name }}</h2>
                            <h3>{{ $knowledge->desc }}</h3>
                            <a class="fr__btn" href="{{ route('public.knowledge', ['slug' => $knowledge->slug]) }}">Xem
                                thêm</a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                        <div class="prize__inner">
                            <div class="prize__thumb">
                                <img src="{{asset('storage/knowledges/'. $knowledge->image)}}"
                                     alt="{{ $knowledge->name }}">
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
    <!-- End Prize Good Area -->
    <!-- Start Blog Area -->
    <section class="htc__blog__area bg__white ptb--100">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section__title--2 text-center">
                        <h2 class="title__line">TIN TỨC</h2>
                        <p>Tin tức về sản phẩm và cách sử dụng</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="ht__blog__wrap clearfix">
                    <!-- Start Single Blog -->
                    @forelse($news as $newspaper)
                        <div class="col-md-6 col-lg-4 col-sm-6 col-xs-12">
                            <div class="blog">
                                <div class="blog__thumb">
                                    <a href="{{ route('public.news.detail',  ['slug' => $newspaper->slug]) }}">
                                        <img src="{{asset('storage/news/'. $newspaper->image)}}"
                                             alt="{{ $newspaper->name }}}">
                                    </a>
                                </div>
                                <div class="blog__details">
                                    <div class="bl__date">
                                        <span>{{ date_format(new DateTime($newspaper->updated_at), 'd-m-Y') }}</span>
                                    </div>
                                    <h2>
                                        <a href="{{ route('public.news.detail',  ['slug' => $newspaper->slug]) }}">{{ $newspaper->name }}</a>
                                    </h2>
                                    <p class="summary_public">{{ $newspaper->summary }}</p>
                                    <div class="blog__btn">
                                        <a href="{{ route('public.news.detail',  ['slug' => $newspaper->slug]) }}">Xem thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p>@lang('messages.no_data')</p>
                @endforelse
                <!-- End Single Blog -->
                </div>
            </div>
        </div>
    </section>
    <!-- End Blog Area -->
    <!-- End Product Area -->
    <!-- Start Testimonial Area -->
    <section class="htc__testimonial__area bg__cat--4">
        <div class="container">
            <h2 style="padding-top: 30px;" class="title__line text-center">DỰ ÁN ĐANG THI CÔNG</h2>
            <div class="row">
                <div class="ht__testimonial__activation clearfix">
                    <!-- Start Single Testimonial -->
                    @forelse(\App\Model\ProjectUnderConstruction::inRandomOrder()->take(DEFAULT_GET_ELEMENT)->get() as $puc)
                        <div class="col-lg-6 col-md-6 single__tes">
                            <div class="testimonial">
                                <div class="testimonial__thumb">
                                    <img src="{{asset('storage/projects_under_construction/'. $puc->image)}}"
                                         alt="{{ $puc->name }}">
                                </div>
                                <div class="testimonial__details">
                                    <h3 style="font-weight: bold">
                                        <a href="{{ route('public.project_building.detail', ['slug' => $puc->slug ]) }}">{{ $puc->name }}</a>
                                    </h3>
                                    <p style="overflow: hidden; text-overflow: ellipsis;  white-space: nowrap; width: 350px!important;">{{ $puc->summary }}</p>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p>Không có dữ liệu</p>
                @endforelse
                <!-- End Single Testimonial -->

                </div>
            </div>
        </div>
    </section>
    <!-- End Testimonial Area -->
    <!-- Start Top Rated Area -->

    <!-- End Top Rated Area -->
    <!-- Start Brand Area -->
    <!-- End Brand Area -->

    <!-- End Banner Area -->
@endsection
