@extends('frontend.layouts.app')

@section('title',"$knowledge->name | Công ty cổ phần công nghệ LEDONE Việt Nam")


@section('content')
    <div class="ht__bradcaump__area"
         style="background: rgba(0, 0, 0, 0) url('{{ asset('storage/knowledges/'. $knowledge->image) }}') no-repeat scroll center center / cover ;">
        <div class="ht__bradcaump__wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="bradcaump__inner">
                            <nav class="bradcaump-inner">
                                <a class="breadcrumb-item" href="{{ route('public.home.index') }}">Trang chủ</a>
                                <span class="brd-separetor"><i class="zmdi zmdi-chevron-right"></i></span>
                                <span class="breadcrumb-item active">{{ $knowledge->name }}</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="htc__blog__area bg__white ptb--100">
        <div class="container">
            <div class="row">
                <div class="ht__blog__wrap blog--page clearfix">
                    <!-- Start Single Blog -->
                    @forelse($newspapers as $news)
                        <div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
                            <div class="blog">
                                <div class="blog__thumb">
                                    <a href="{{ route('public.news.detail', ['slug' => $news->slug]) }}">
                                        <img src="{{ asset('storage/news/'. $news->image) }}" alt="{{ $news->name }}">
                                    </a>
                                </div>
                                <div class="blog__details">
                                    <div class="bl__date">
                                        <span>{{ date_format(new DateTime($news->updated_at), 'd-m-Y') }}</span>
                                    </div>
                                    <h2><a href="{{ route('public.news.detail', ['slug' => $news->slug]) }}">{{ $news->name }}</a></h2>
                                    <p>{{ $news->summary }}</p>
                                    <div class="blog__btn">
                                        <a href="{{ route('public.news.detail', ['slug' => $news->slug]) }}">Xem thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                @empty
                @endforelse
                <!-- End Single Blog -->
                </div>
            </div>
            <!-- Start PAgenation -->
            <div class="row">
                <div class="col-xs-12 text-center">
                    {{ $newspapers->links() }}
                </div>
            </div>
            <!-- End PAgenation -->
        </div>
    </section>
@endsection