@extends('frontend.layouts.app')

@if(!empty($category))
@section('title',"$category->name | Công ty cổ phần công nghệ LEDONE Việt Nam")
@else
@section('title',"Công ty cổ phần công nghệ LEDONE Việt Nam")
@endif

@section('content')
    <!-- Start Bradcaump area -->
    @if(!empty($category))
    <div class="ht__bradcaump__area"
         style="background: rgba(0, 0, 0, 0) url('{{asset('storage/categories/'. $category->image)}}') no-repeat scroll center center / cover ;">
        <div class="ht__bradcaump__wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="bradcaump__inner">
                            <nav class="bradcaump-inner">
                                <a class="breadcrumb-item" href="{{ route('public.home.index')}}">Trang chủ</a>
                                <span class="brd-separetor"><i class="zmdi zmdi-chevron-right"></i></span>
                                <span class="breadcrumb-item active">{{ $category->name }}</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <!-- End Bradcaump area -->
    <!-- Start Product Grid -->
    <section class="htc__product__grid bg__white ptb--100">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-lg-push-3 col-md-9 col-md-push-3 col-sm-12 col-xs-12">
                    <div class="htc__product__rightidebar">
                        <div class="htc__grid__top">
                            <div class="htc__select__option">
                                <form id="sort_product"
                                      action="{{ route('public.category', ['slug' => $category->slug]) }}" method="GET">
                                    <select class="ht__select" id="select_sort_product" name="sort">
                                        <option value="0" {{\App\Model\Product::SORT_DEFAUL == request()->get('sort') ? 'selected' : ''}}>
                                            Sắp xếp mạc định
                                        </option>
                                        <option value="1" {{\App\Model\Product::SORT_ASC == request()->get('sort') ? 'selected' : ''}}>
                                            Sắp xếp theo giá từ giảm tới tăng
                                        </option>
                                        <option value="2" {{\App\Model\Product::SORT_DESC == request()->get('sort') ? 'selected' : ''}}>
                                            Sắp xếp theo giá từ tăng xuống giảm
                                        </option>
                                        <option value="3" {{\App\Model\Product::SORT_RATING == request()->get('sort') ? 'selected' : ''}}>
                                            Đánh gía cao
                                        </option>
                                    </select>
                                    @csrf
                                </form>
                            </div>
                            <div class="ht__pro__qun">
                                <span>Tổng tất cả sản phẩm: {{ count($products)}} sản phẩm</span>
                            </div>
                            <!-- Start List And Grid View -->
                            <ul class="view__mode" role="tablist">
                                <li role="presentation" class="grid-view active"><a href="#grid-view" role="tab"
                                                                                    data-toggle="tab"><i
                                                class="zmdi zmdi-grid"></i></a></li>
                                <li role="presentation" class="list-view"><a href="#list-view" role="tab"
                                                                             data-toggle="tab"><i
                                                class="zmdi zmdi-view-list"></i></a></li>
                            </ul>
                            <!-- End List And Grid View -->
                        </div>
                        <!-- Start Product View -->
                        <div class="row">
                            <div class="shop__grid__view__wrap">
                                <div role="tabpanel" id="grid-view"
                                     class="single-grid-view tab-pane fade in active clearfix">
                                @foreach($products as $product)
                                    <!-- Start Single Product -->
                                        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                                            <div class="category">
                                                <div class="ht__cat__thumb">
                                                    <a href="{{ route('public.product', ['slug' => $product->slug])}}">
                                                        <img src="{{ asset('storage/products/'. $product->image)}}"
                                                             alt="{{ $product->name }}">
                                                    </a>
                                                </div>

                                                <div class="fr__product__inner">
                                                    <h4>
                                                        <a href="{{ route('public.product', ['slug' => $product->slug])}}">{{ $product->name }}</a>
                                                    </h4>
                                                    <ul class="fr__pro__prize">
                                                        @if($product->promotion == App\Model\Product::PROMOTION)
                                                            <li class="old__prize">{{ number_format($product->price, 0 , ',', '.' )}}
                                                                vnđ
                                                            </li>
                                                        @endif
                                                        <li>{{ number_format($product->price - $product->sale_off, 0 , ',', '.')}}
                                                            vnđ
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                @endforeach
                                <!-- End Single Product -->
                                </div>
                                <div role="tabpanel" id="list-view" class="single-grid-view tab-pane fade clearfix">
                                    <div class="col-xs-12">
                                        <div class="ht__list__wrap">
                                        @foreach($products as $product)
                                            <!-- Start List Product -->
                                                <div class="ht__list__product">
                                                    <div class="ht__list__thumb">
                                                        <a href="{{ route('public.product', ['slug' => $product->slug])}}"><img
                                                                    src="{{ asset('storage/products/'. $product->image)}}"
                                                                    alt="{{ $product->name}}"></a>
                                                    </div>
                                                    <div class="htc__list__details">
                                                        <h2>
                                                            <a href="{{ route('public.product', ['slug' => $product->slug])}}">{{ $product->name}} </a>
                                                        </h2>
                                                        <ul class="pro__prize">
                                                            @if($product->promotion == App\Model\Product::PROMOTION)
                                                                <li class="old__prize">{{ number_format($product->price, 0 , ',', '.')}}
                                                                    vnđ
                                                                </li>
                                                            @endif
                                                            <li>{{ number_format($product->price - $product->sale_off, 0 , ',', '.')}}
                                                                vnđ
                                                            </li>
                                                        </ul>
                                                        <ul class="rating">
                                                            <li><i class="icon-star icons"></i></li>
                                                            <li><i class="icon-star icons"></i></li>
                                                            <li><i class="icon-star icons"></i></li>
                                                            <li><i class="icon-star icons"></i></li>
                                                            <li><i class="icon-star icons"></i></li>
                                                        </ul>
                                                        <p>{{ $product->summary }}</p>

                                                    </div>
                                                </div>
                                        @endforeach
                                        <!-- End List Product -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Product View -->
                    </div>
                    <!-- Start Pagenation -->
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            {{ $products->links() }}
                        </div>
                    </div>
                    <!-- End Pagenation -->
                </div>
                <div class="col-lg-3 col-lg-pull-9 col-md-3 col-md-pull-9 col-sm-12 col-xs-12 smt-40 xmt-40">
                    <div class="htc__product__leftsidebar">
                        <!-- Start Prize Range -->
                        <div class="htc-grid-range">
                            <h4 class="title__line--4">Giá sản phẩm</h4>
                            <div class="content-shopby">
                                <div class="price_filter s-filter clear">
                                    <form action="{{ route('public.category', ['slug' => $category->slug]) }}" method="GET">
                                        @csrf
                                        <div id="slider-range"></div>
                                        <div class="slider__range--output">
                                            <div class="price__output--wrap">
                                                <div class="price--output">
                                                    <span>Giá sản phẩm :</span><input type="text" id="amount" readonly>
                                                    <input type="hidden" name="sort_price_start" id="sort_price_start" value="" />
                                                    <input type="hidden" name="sort_price_end" id="sort_price_end" value="" />
                                                </div>
                                                <div class="price--filter">
                                                    <button type="submit" class="btn btn-defaul">Lọc</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- End Prize Range -->
                        <!-- Start Category Area -->
                        <div class="htc__category">
                            <h4 class="title__line--4">Danh mục sản phẩm liên quan</h4>
                            <ul class="ht__cat__list">
                                @foreach($categories as $category)
                                    @if(count($category->products))
                                        <li>
                                            <a href="{{ route('public.category', ['slug' => $category->slug ])}}">{{ $category->name }}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                        <!-- End Category Area -->

                        <!-- Start Tag Area -->
                        <div class="htc__tag">
                            <h4 class="title__line--4">tags</h4>
                            <ul class="ht__tag__list">
                                @foreach($tags as $tag)
                                    @if(count($tag->products))
                                        <li><a href="{{ route('public.tags',['slug' => $tag->slug])}}">{{ $tag->name }}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                        <!-- End Tag Area -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Product Grid -->
@endsection