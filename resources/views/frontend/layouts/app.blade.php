<!doctype html>
<html class="no-js" lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('frontend/images/logo/logoledone.png')}}">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:type" content="object" />
    <meta property="og:title" content="Màn hình LCD quảng cáo Archives - Công ty cổ phần công nghệ LEDONE Việt Nam" />
    <meta property="og:site_name" content="Công ty cổ phần công nghệ LEDONE Việt Nam" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="Màn hình LCD quảng cáo Archives - Công ty cổ phần công nghệ LEDONE Việt Nam" />
    <meta name="twitter:site" content="@quangcaohiendai" />
    <!-- All css files are included here. -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Mono|Source+Sans+Pro&display=swap" rel="stylesheet"> 
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">
    <!-- Owl Carousel min css -->
    <link rel="stylesheet" href="{{asset('frontend/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/owl.theme.default.min.css')}}">
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="{{asset('frontend/css/core.css')}}">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="{{asset('frontend/css/shortcode/shortcodes.css')}}">
    <!-- Theme main style -->
    <link rel="stylesheet" href="{{asset('frontend/style.css')}}">
    <!-- Responsive css -->
    <link rel="stylesheet" href="{{asset('frontend/css/responsive.css')}}">
    <!-- User style -->
    <link rel="stylesheet" href="{{asset('frontend/css/custom.css')}}">
    <link href="{{ asset('frontend/fontawesome-free-5.10.2-web/css/all.css') }}" rel="stylesheet">
    <!--load all styles -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('frontend/images/icon_coppy.png') }}"/>

    <!-- Modernizr JS -->
    <script src="{{asset('frontend/js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Hepta+Slab|PT+Serif|Roboto|Source+Sans+Pro&display=swap" rel="stylesheet">
</head>

<body>
<!-- Body main wrapper start -->
<div class="wrapper">
    <!-- start topbar -->
    <header class="topbar" style="min-height: 0px !important;">
        <!-- start container -->
        <div class="container">
            <ul class="topbar-menu">
                @auth
                    <li>Xin chào : {{auth()->user()->name}}</li>
                    <li><a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Đăng xuất
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                    @if(auth()->check())
                        <li><a href="{{route('admin.categories.index')}}" title="Donations">Trang quản trị</a></li>
                    @endif
            </ul>
            <div class="header-info-col">
                <i class="fab fa-adversal"></i>
                <strong>Liên hệ: </strong>
            </div>
            <div class="header-info-col">
                <i class="fa fa-phone"></i>
                <strong>0399992035</strong>
            </div>
            <div class="header-info-col">
                <i class="fa fa-envelope-o"></i>
                <strong><a href="mailto:caotuanvu198@gmail.com"
                           title="caotuanvu198@gmail.co">caotuanvu198@gmail.co</a></strong>
            </div>
            <div class="header-info-col">
                <i class="fa fa-map-marker"></i>
                <strong>Thành phố Huế</strong>
            </div>
            @endauth
        </div>
        <!-- end container -->
    </header>
    <!-- Start Header Style -->
    <header id="htc__header" class="htc__header__area header--one">
        <!-- Start Mainmenu Area -->
        <div id="sticky-header-with-topbar" class="mainmenu__wrap sticky__header">
            <div class="container">
                <div class="row">
                    <div class="menumenu__container clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-5">
                            <div class="logo">
                                <a href="{{ route('public.home.index') }}"><img
                                            src="{{asset('frontend/images/logo/logoledone.png')}}"
                                            alt="Công ty cổ phần công nghệ LEDONE Việt Nam"></a>
                            </div>
                        </div>
                        <div class="col-md-7 col-lg-8 col-sm-5 col-xs-3">
                            <nav class="main__menu__nav hidden-xs hidden-sm">
                                <ul class="main__menu">
                                    <li class="drop"><a href="{{ route('public.home.index') }}">TRANG CHỦ</a></li>
                                    <li class="drop"><a href="{{ route('public.introduction') }}">GIỚI THIỆU</a>
                                        <ul class="dropdown">
                                            <li><a href="{{ route('public.policy.regulation') }}">Chính sách quy định</a></li>
                                        </ul>
                                    </li>
                                    <li class="drop"><a href="{{ route('public.home.index') }}">SẢN PHẨM</a>
                                        <ul class="dropdown">
                                            @foreach(\App\Model\Category::all() as $category)
                                                @if(count($category->products))
                                                    <li>
                                                        <a href="{{ route('public.category', ['slug' => $category->slug]) }}">{{ $category->name }}</a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                    <li class="drop"><a href="#">KIẾN THỨC</a>
                                        <ul class="dropdown">
                                            @foreach(\App\Model\Knowledge::all() as $knowledge)
                                                @if(count($knowledge->news))
                                                    <li>
                                                        <a href="{{ route('public.knowledge', ['slug' => $knowledge->slug]) }}">{{ $knowledge->name }}</a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                    <li><a href="{{ route('public.project.building.index') }}">DỰ ÁN THI CÔNG</a></li>
                                </ul>
                            </nav>

                            <div class="mobile-menu clearfix visible-xs visible-sm">
                                <nav id="mobile_dropdown">
                                    <ul>
                                        <li><a href="{{ route('public.home.index') }}">TRANG CHỦ</a></li>
                                        <li><a href="{{ route('public.introduction') }}">GIỚI THIỆU</a>
                                            <ul style="height: auto !important;">
                                                <li><a href="{{ route('public.policy.regulation') }}">Chính sách quy
                                                        định</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="{{ route('public.home.index') }}">SẢN PHẨM</a>
                                            <ul style="height: auto !important;">
                                                @foreach(\App\Model\Category::all() as $category)
                                                @if(count($category->products))
                                                <li><a href="{{ route('public.category', ['slug' => $category->slug]) }}">{{ $category->name }}</a></li>
                                                @endif
                                                @endforeach
                                            </ul>
                                        </li>
                                        <li><a href="#">KIẾN THỨC</a>
                                            <ul style="height: auto !important;">
                                                @foreach(\App\Model\Knowledge::all() as $knowledge)
                                                @if(count($knowledge->news))
                                                    <li><a href="{{ route('public.knowledge', ['slug' => $knowledge->slug]) }}">{{ $knowledge->name }}</a></li>
                                                @endif
                                                @endforeach
                                            </ul>
                                        </li>
                                        <li><a href="contact.html">PHẦN MỀM LED</a></li>
                                        <li><a href="{{ route('public.project.building.index') }}">DỰ ÁN THI CÔNG</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        @if(!auth()->check())
                        <div class="col-md-3 col-lg-2 col-sm-4 col-xs-4">
                            <div class="header__right">
                                <div class="header__account">
                                    <a data-toggle="tooltip" title="Đăng nhập !" href="{{route('login')}}">
                                        <i style="color: #fff8f8; border-right: none;" class="icon-user icons"></i>
                                     </a>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="mobile-menu-area"></div>
            </div>
        </div>
        <!-- End Mainmenu Area -->
    </header>
    <!-- End Header Area -->

    <div class="body__overlay"></div>
    <!-- Start Offset Wrapper -->

@yield('content')

<!-- Start Footer Area -->
    <footer id="htc__footer">
        <!-- Start Footer Widget -->
        <div class="footer__container bg__cat--1">
            <div class="container">
                <div class="row">
                    <!-- Start Single Footer Widget -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="footer">
                            <h2 class="title__line--2">THÔNG TIN LIÊN HỆ</h2>
                            <div class="ft__details">
                                <div class="ft__inner">
                                    <ul class="ft__list">
                                        <li><a>CÔNG TY CỔ PHẦN CÔNG NGHỆ LEDONE VIỆT NAM</a></li>
                                        <li><a>Trụ sở : 104 Ngõ 360 Xã Đàn, P. Trung Phụng, Q. Đống Đa,
                                                TP. Hà Nội</a></li>
                                        <li><a>Hotline :0936783868 ( Mr. Hoàng ) 0888052599 ( Mr Hiểu
                                                Minh )</a></li>
                                        <li><a>Cho thuê màn hình led : 0844653222 ( Mr Tuấn Kiệt )</a></li>
                                        <li><a>Miền Trung : Số 235 Tôn Đản , Q. Cẩm Lệ, TP. Đà Nẵng</a></li>
                                        <li><a>Điện thoại : 0888052599 ( Mr Hiểu Minh ) 0936783868 ( Mr. Hoàng )</a>
                                        </li>
                                        <li><a>Miền Nam : Số 1/24 Nguyễn Văn Dung, P6, Q. Gò Vấp, TP. HCM</a></li>
                                        <li><a>Email : CEO.ledone@gmail.com</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Footer Widget -->
                    <!-- Start Single Footer Widget -->
                    <div class="col-md-6 col-sm-6 col-xs-12 xmt-40">
                        <div class="footer">
                            <h2 class="title__line--2">BẢN ĐỒ CHỈ ĐƯỜNG</h2>
                            <div class="textwidget custom-html-widget">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3647.7934052062865!2d105.83258706661688!3d21.015021337869584!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135abc18cf570bb%3A0xd752e7308b5ff7be!2zQ8O0bmcgdHkgY-G7lSBwaOG6p24gY8O0bmcgbmdo4buHIExFRE9ORSBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1527838375061"
                                        width="100%" height="270" frameborder="0" style="border:0"
                                        allowfullscreen=""></iframe>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Footer Widget -->
                </div>
            </div>
        </div>
        <!-- End Footer Widget -->
        <!-- Start Copyright Area -->
        <div class="htc__copyright bg__cat--5">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p style="padding: 30px; font-weight: bold; color: #666666">Copyright 2019 © LEDONE</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Copyright Area -->
    </footer>
    <!-- End Footer Style -->
</div>
<!--Start of Tawk.to Script (0.3.3)-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {};
    var Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5b343a7fd0b5a54796823e97/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script (0.3.3)-->
<!-- Body main wrapper end -->

<!-- Placed js at the end of the document so the pages load faster -->
@yield('js')
<!-- jquery latest version -->
<script src="{{asset('frontend/js/vendor/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap framework js -->
<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
<!-- All js plugins included in this file. -->
<script src="{{asset('frontend/js/plugins.js')}}"></script>
<script src="{{asset('frontend/js/slick.min.js')}}"></script>
<script src="{{asset('frontend/js/owl.carousel.min.js')}}"></script>
<!-- Waypoints.min.js. -->
<script src="{{asset('frontend/js/waypoints.min.js')}}"></script>
<!-- Main js file that contents all jQuery plugins activation. -->
<script src="{{asset('frontend/js/main.js')}}"></script>
<script src="{{asset('frontend/js/custom.js')}}"></script>
<script src="{{asset('frontend/js/review.js')}}"></script>
<link href="{{ asset('frontend/fontawesome-free-5.10.2-web/js/all.js') }}" rel="stylesheet"> <!--load all js -->
<script src="{{ asset('js/share.js') }}"></script>
</body>

</html>
