@extends('adminlte::page')

@section('title', 'Thêm tin tức mới')

@section('content_header')
    <h1>Thêm tin tức mới</h1>
@stop

@section('content')

    <form action="{{ route('admin.news.add') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="name">Tên: <span class="color-red">*</span></label>
            <input type="text" class="form-control <?php echo $errors->has('name') ? 'input-error' : '';?>" value="{{ old('name') }}" name="name" id="name">
        </div>
        <div class="form-group">
            <label for="name">Hình ảnh: <span class="color-red">*</span></label>
            <input type="file" class="form-control <?php echo $errors->has('image') ? 'input-error' : '';?>" value="{{ old('image') }}" name="image">
        </div>
        <div class="form-group">
            <label for="name">Tóm tắt: <span class="color-red">*</span></label> <br>
            <textarea name="summary" class="form-control <?php echo $errors->has('summary') ? 'input-error' : '';?>" rows="5" id="summary"> {{ old('summary')}} </textarea>
        </div>
        <div class="form-group">
            <label for="name">Thể loại tin: <span class="color-red">*</span></label>
            <select class="form-control data-select-all <?php echo $errors->has('knowledge_id') ? 'input-error' : '';?>" name="knowledge_id">
                @if(count($knowledges) > 0)
                    <option value="0">{{trans('messages.choose_news')}}</option>
                    @foreach ($knowledges as $knowledge)
                        <option value="{{$knowledge->id}}" {{old('knowledge_id') == $knowledge->id ?'selected':''}}>{{$knowledge->name}}</option>
                    @endforeach
                @else
                    <option value="">Không có thẻ tag nào</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label for="name">Mô tả: <span class="color-red">*</span></label> <br>
            <textarea name="desc" id="text" rows="10" cols="80">
                    {{ old('desc')}}
            </textarea>
        </div>
        
        <button type="submit" class="btn btn-success">Thêm mới</button>
    </form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop