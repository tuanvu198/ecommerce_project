@extends('adminlte::page')

@section('title', 'Quản lý tin tức')

@section('content_header')
    <h1 class="inline">Danh sách tin tức</h1>
    <a class="btn btn-info pull-right" href="{{ route('admin.news.add') }}">Thêm tin tức mới</a>
@stop

@section('content')
    <p>Tìm kiếm</p>
    <div class="container-fluid">
        <div class="row">
            <form>
                <div class="input-group">
                    <input type="text" name="news" class="form-control" placeholder="Search">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordred table-striped table-bordered">
                    <caption>Danh sách</caption>
                    <thead class="panel-title">
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Tiêu đề</th>
                            <th scope="col">Hình ảnh</th>
                            <th scope="col">Tóm tắt</th>
                            <th scope="col">Thể loại tin</th>
                            <th scope="col" class="text-center">Xem phản hồi</th>
                            <th scope="col" class="text-center">Xóa</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($news as $newspaper)
                        <tr>
                        <th scope="row">{{ $newspaper->id }}</th>
                            <td>
                            <a href="{{ route('admin.news.edit', ['slug' => $newspaper->slug]) }}">{{ $newspaper->name }}</a>
                            </td>
                            <td>
                            <img style="max-width: 200px; height: 100px;-o-object-fit: contain;" src="{{asset('storage/news/'. $newspaper->image )}}" alt="{{ $newspaper->name }}">
                            </td>
                            <td>
                            <p class="summary">{{ $newspaper->summary }}</p>
                            </td>
                            <td>
                                 {{ $newspaper->knowledge->name }}
                            </td>
                            <th class="text-center"><a data-toggle="tooltip" title="Xem phản hồi !" href="{{ route('admin.feedbacks.index', ['id'=> $newspaper->id]) }}"><i class="fas fa-directions"></i></a></th>
                            <td class="text-center">
                                <form action="{{route('admin.news.delete',['slug' => $newspaper->slug])}}" class="form-edit formConfirmDeleteCat" method="post">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-danger submitDelete" value="Xóa">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="12" class="text-center">@lang('messages.no_data')</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>

    </div>

@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')
    <script src="{{asset('/js/ckeditor/ckeditor.js')}}"></script>
@stop