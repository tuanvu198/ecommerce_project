@extends('adminlte::page')

@section('title', 'Chỉnh sửa dự án đang thi công')

@section('content_header')
    <h1>Chỉnh sửa dự án đang thi công</h1>
@stop

@section('content')

    <form action="{{ route('admin.projects_under_construction.update', ['slug' => $project_building->slug]) }}" method="POST" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="name">Tên: <span class="color-red">*</span></label>
            <input type="text" class="form-control <?php echo $errors->has('name') ? 'input-error' : '';?>" value="{{ $project_building->name }}" name="name" id="name">
        </div>
        <div class="form-group">
            <label for="name">Hình ảnh: <span class="color-red">*</span></label>
            <input type="file" class="form-control <?php echo $errors->has('image') ? 'input-error' : '';?>" value="{{ old('image') }}" name="image">
            <p><img src="{{ asset('storage/projects_under_construction/'. $project_building->image)}}" class="img-thumbnail" alt="{{ $project_building->name }}" style="max-width: 200px; height: 100px;-o-object-fit: contain;"></p>
        </div>
        <div class="form-group">
            <label for="name">Tóm tắt: <span class="color-red">*</span></label> <br>
            <textarea name="summary" class="form-control <?php echo $errors->has('summary') ? 'input-error' : '';?>" rows="5" id="summary"> {{ $project_building->summary}} </textarea>
        </div>
        <div class="form-group">
            <label for="name">Mô tả: <span class="color-red">*</span></label> <br>
            <textarea name="desc" id="text" rows="10" cols="80">
                    {{ $project_building->desc }}
            </textarea>
        </div>
        
        <button type="submit" class="btn btn-success">Chỉnh sửa</button>
    </form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop