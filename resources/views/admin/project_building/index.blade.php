@extends('adminlte::page')

@section('title', 'Quản lý dự án thi công')

@section('content_header')
    <h1 class="inline">Danh sách các dự án đang thi công</h1>
    <a class="btn btn-info pull-right" href="{{ route('admin.projects_under_construction.add') }}">Thêm dự án mới</a>
@stop

@section('content')
    <p>Tìm kiếm</p>
    <div class="container-fluid">
        <div class="row">
            <form>
                <div class="input-group">
                    <input type="text" name="project_under_construction" class="form-control" placeholder="Search">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordred table-striped table-bordered">
                    <caption>Danh sách</caption>
                    <thead class="panel-title">
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Tên</th>
                            <th scope="col">Hình ảnh</th>
                            <th scope="col">Tóm tắt</th>
                            <th scope="col">Sửa</th>
                            <th scope="col">Xóa</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($projects_under_construction as $project_under_construction)
                        <tr>
                        <th scope="row">{{ $project_under_construction->id }}</th>
                            <td>
                            {{ $project_under_construction->name }}
                            </td>   
                            <td>
                            <img class="image-project-building" src="{{asset('storage/projects_under_construction/'. $project_under_construction->image )}}" alt="{{ $project_under_construction->name }}">
                            </td>
                            <td>
                            <p class="summary">{{ $project_under_construction->summary }}</p>
                            </td>
                            <td>
                                <a href="{{ route('admin.projects_under_construction.edit',['slug' => $project_under_construction->slug]) }}"
                                   class="btn btn-info"><i class="fas fa-edit">Sửa</i></a>
                            </td>
                            <td>
                                <form action="{{route('admin.projects_under_construction.delete',['slug' => $project_under_construction->slug])}}" class="form-edit formConfirmDeleteCat" method="post">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-danger submitDelete" value="Xóa">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="12" class="text-center">@lang('messages.no_data')</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>

    </div>

@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')
    <script src="{{asset('/js/ckeditor/ckeditor.js')}}"></script>
@stop