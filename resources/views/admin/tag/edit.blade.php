@extends('adminlte::page')

@section('title', 'Sửa tag')

@section('content_header')
    <h1>Sửa tag</h1>
@stop

@section('content')
    <form action="{{ route('admin.tags.update', ['slug' => $tag->slug]) }}" method="POST">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="name">Tên: <span class="color-red">*</span></label>
            <input type="text" value=" {{ $tag->name  }} " class="form-control" name="name" id="name">
        </div>
        <button type="submit" class="btn btn-success">Cập nhật</button>
    </form>
@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')

@stop