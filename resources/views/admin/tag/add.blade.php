@extends('adminlte::page')

@section('title', 'Thêm mới tag')

@section('content_header')
    <h1>Thêm tag</h1>
@stop

@section('content')

    <form action="{{ route('admin.tags.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Tên: <span class="color-red">*</span></label>
            <input type="text" class="form-control" value="{{old('name')}}" name="name" id="name">
        </div>
        <button type="submit" class="btn btn-success">Thêm mới</button>
    </form>

@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')

@stop