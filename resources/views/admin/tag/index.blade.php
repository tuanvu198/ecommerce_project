@extends('adminlte::page')


@section('title', 'quản lý tag')

@section('content_header')
    <h1 class="inline">Danh sách các tag</h1>
    <a class="btn btn-info pull-right" href="{{ route('admin.tags.add') }}">Thêm tag mới</a>
@stop

@section('content')
    <p>Tìm kiếm</p>
    <div class="container-fluid">
        <div class="row">
        <form action="{{ route('admin.tags.index') }}" method="GET">
                <div class="input-group">
                    <input name="tag" type="text" class="form-control" placeholder="Search">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordred table-striped table-bordered">
                    <caption>Danh sách</caption>
                    <thead class="panel-title">
                    <tr>
                        <th scope="col">Stt</th>
                        <th scope="col">Tên</th>
                        <th scope="col">Sửa</th>
                        <th scope="col">Xóa</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $stt = 1;?>
                    @forelse($tags as $tag)
                        <tr>
                            <th scope="row">{{ $stt }}</th>
                            <td>{{ $tag->name }}</td>
                            <td>
                                <a href="{{ route('admin.tags.edit',['slug' => $tag->slug]) }}"
                                   class="btn btn-info"><i class="fas fa-edit">Sửa</i></a>
                            </td>
                            <td>
                                <form action="{{route('admin.tags.delete',['slug' => $tag->slug])}}"
                                      class="form-edit formConfirmDeleteTag" method="post">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-danger submitDelete" value="Xóa">
                                </form>
                            </td>
                        </tr>
                        <?php $stt++;?>
                    @empty
                        <tr>
                            <td colspan="12" class="text-center">@lang('messages.no_data')</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div>

    </div>

@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')
    <script src="{{asset('/js/ckeditor/ckeditor.js')}}"></script>
@stop