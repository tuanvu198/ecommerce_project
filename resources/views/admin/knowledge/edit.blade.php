@extends('adminlte::page')

@section('title', 'Sửa kiến thức')

@section('content_header')
	<h1 class="inline">Sửa kiến thức</h1>
@stop

@section('content')
	<form action="{{ route('admin.knowledges.update', ['slug' => $knowledge->slug]) }}" method="POST" enctype="multipart/form-data">
		@method('PUT')
		@csrf
		<div class="form-group">
	    	<label for="name1">Tên: <span class="color-red">*</span></label>
	    	<input type="text" value="{{ $knowledge->name }}" class="form-control <?php echo $errors->has('name') ? 'input-error' : '';?>" id="name" name="name" placeholder="Name" required>
	  	</div>
		<div class="form-group">
			<label for="name">Hình ảnh : <span class="color-red">*</span></label>
			<input type="file" class="form-control <?php echo $errors->has('image') ? 'input-error' : '';?>" value="{{old('image')}}" name="image" id="image">
			<p><img src="{{ asset('storage/knowledges/'. $knowledge->image)}}" class="img-thumbnail" alt="{{ $knowledge->name }}" style="max-width: 200px; height: 100px;-o-object-fit: contain;"></p>
		</div>
		<div class="form-group">
            <textarea name="desc" class="form-control"  rows="10" cols="100">
                {{ $knowledge->desc  }}
            </textarea>
		</div>

	  	<button type="submit" class="btn btn-success">Cập nhật</button>
	</form>
@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')
    
@stop