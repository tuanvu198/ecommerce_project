@extends('adminlte::page')

@section('title', 'Quản lý kiến thức')

@section('content_header')
    <h1 class="inline">Danh sách kiến thức</h1>
    <a class="btn btn-info pull-right" href="{{ route('admin.knowledges.add') }}">Thêm kiến thức mới</a>
@stop

@section('content')

    <p>Tìm kiếm</p>

    <div class="container-fluid">
        <div class="row">
            <form action="{{ route('admin.knowledges.index') }}" method="GET">
                <div class="input-group">
                    <input name="knowledges" type="text" class="form-control" placeholder="Search">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordred table-striped table-bordered">
                    <caption>Danh sách</caption>
                    <thead class="panel-title">
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Tên</th>
                            <th class="text-center" scope="col">Sửa</th>
                            <th class="text-center" scope="col">Xóa</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($knowledges as $knowledge)
                        <tr>
                            <th scope="row">{{ $knowledge->id }}</th>
                            <td>{{ $knowledge->name }}</td>
                            <td class="text-center">
                                <a href="{{ route('admin.knowledges.edit', ['slug' => $knowledge->slug]) }}" class="btn btn-info"><i class="fas fa-edit">Sửa</i></a>
                            </td>
                            <td class="text-center">
                                <form action="{{ route('admin.knowledges.delete', ['slug' => $knowledge->slug]) }}" class="form-edit formConfirmDeleteKnow" method="post">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-danger submitDelete" value="Xóa">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="12" class="text-center">@lang('messages.no_data')</td>
                        </tr> 
                    </tbody>
                    @endforelse
                </table>
            </div>
        </div>

    </div>

@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')
    <script src="{{asset('/js/ckeditor/ckeditor.js')}}"></script>
@stop