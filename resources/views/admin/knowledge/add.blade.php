@extends('adminlte::page')

@section('title', 'Thêm kiến thức')

@section('content_header')
    <h1>Thêm kiến thức</h1>
@stop

@section('content')

    <form action="{{ route('admin.knowledges.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="name">Tên: <span class="color-red">*</span></label>
            <input type="text" class="form-control <?php echo $errors->has('name') ? 'input-error' : '';?>" value="{{old('name')}}" name="name">
        </div>
        <div class="form-group">
            <label for="name">Hình ảnh : <span class="color-red">*</span></label>
            <input type="file" class="form-control <?php echo $errors->has('image') ? 'input-error' : '';?>" value="{{old('image')}}" name="image" id="image">
        </div>
        <div class="form-group">
            <textarea name="desc" rows="10" cols="80" class="form-control <?php echo $errors->has('desc') ? 'input-error' : '';?>">
                {{old('desc')}}
            </textarea>
        </div>

        <button type="submit" class="btn btn-success">Thêm mới</button>
    </form>

@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')
@stop