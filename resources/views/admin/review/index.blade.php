@extends('adminlte::page')

@section('title', 'đánh giá sản phẩm')

@section('content_header')
<h1 class="inline">Đánh giá sản phẩm - <span class="color-red">{{ $product->name }}</span></h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordred table-striped table-bordered text-center">
                    <caption>Danh sách</caption>
                    <thead class="panel-title">
                    <tr>
                        <th scope="col">Stt</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Đánh giá</th>
                        <th scope="col">Nội dung</th>
                        <th scope="col">Trạng thái</th>
                        <th scope="col">Xóa</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($ratings_product as $rating_product)
                        <tr>
                            <th scope="row">{{ $rating_product->id }}</th>
                            <td>{{ $rating_product->name }}</td>
                            <td>{{ $rating_product->email }}</td>
                            <td>                            
                                <div class="stars starrr" data-rating="0">
                                    @for($i = 0; $i < $rating_product->rating; $i++)
                                        <span class="glyphicon .glyphicon-star-empty glyphicon-star">
                                    </span>
                                    @endfor
                                </div>
                            </td>
                            <td class="summary">
                            <a id="summary-{{ $rating_product->id }}" onclick="openModal('{{ $rating_product->id }}');"  href="#" data-toggle="modal" data-target="#modalContent">{{ $rating_product->content }}</a>
                            </td>
                            <td><a 
                            href="javascript:void(0)" 
                            onclick="changeStatus('{{ $rating_product->id }}');" 
                            data-toggle="tooltip" 
                            title="{{ $rating_product->status == App\Model\Review::PUBLIC ? 'đánh giá được kích hoạt' : 'đánh giá chưa được kích hoạt'}}">
                            <i class="{{ ($rating_product->status == App\Model\Review::PUBLIC) ? 'far fa-check-square fa-lg' : 'far fa-eye-slash'}}" 
                            id="{{ $rating_product->id }}"
                            data-value="{{ $rating_product->id }}"
                            ></i></a></td>
                            <td>
                                <form action="{{route('admin.reviews.delete',['id' => $rating_product->id])}}"
                                      class="form-edit formConfirmDeleteCat" method="post">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-danger submitDelete" value="Xóa">
                                </form>
                            </td>
                        </tr>
                         
                    @empty
                        <tr>
                            <td colspan="12" class="text-center">@lang('messages.no_data')</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        <a class="btn btn-info" href="{{ route('admin.products.index') }}"><i class="fas fa-backward"></i>&nbsp;&nbsp;Back</a>
        </div>
    </div>
    <!-- Modal -->
    <div id="modalContent" class="modal fade" role="dialog">
    </div>
@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">
    <style>
         .animated {
            -webkit-transition: height 0.2s;
            -moz-transition: height 0.2s;
            transition: height 0.2s;
        }
        .stars
        {
            margin: 20px 0;
            font-size: 24px;
            color: #d17581;
        }
    </style>

@stop

@section('js')
<script src="{{asset('/js/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('js/custom_admin.js') }}"></script>
<script>
    function changeStatus(review) {
        var status = $('i#' + review).attr('data-value');
        $.ajax({
            url: "/admin/reviews/change_status/" + review,
            data: {status: status},
            type: "GET",    
            success:function(data){
                var status = data.success;
                $("i#" + review).attr('data-value', status);
                if(status == 0) {
                    $('i#' + review).removeClass('far fa-check-square fa-lg').addClass('far fa-eye-slash');
                    $('i#' + review).parents().attr('title','Đánh giá không được kích hoạt !');
                }else {
                    $('i#' + review).removeClass('far fa-eye-slash').addClass('far fa-check-square fa-lg');
                    $('i#' + review).parents().attr('title','Đánh giá được kích hoạt !');
                }
            },
            error: function(err){
                alert('Đã có lỗi xảy ra, vui lòng tải lại trang !');
            }
        });
    }

    function openModal(review) {
        $.ajax({
            url: "/admin/reviews/detail/" + review,
            type: "GET",    
            success:function(data){
                $('#modalContent').html(data);
            },
            error: function(err){
                alert('Đã có lỗi xảy ra, vui lòng tải lại trang !');
            }
        });
    }
</script>
@stop