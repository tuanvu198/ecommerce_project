@extends('adminlte::page')

@section('title', 'Thêm mới sản phẩm')

@section('content_header')
    <h1>Thêm sản phẩm</h1>
@stop

@section('content')

    <form action="{{ route('admin.products.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="name">Tên: <span class="color-red">*</span></label>
            <input type="text" class="form-control <?php echo $errors->has('name') ? 'input-error' : '';?>" value="{{old('name')}}" name="name">
        </div>
        <div class="form-group">
            <label for="name">Giá: <span class="color-red">*</span></label>
            <input type="text" class="form-control <?php echo $errors->has('price') ? 'input-error' : '';?>" value="{{old('price')}}" name="price">
        </div>
        <div class="form-group">
            <label for="">Trạng thái giá</label>
            <select class="form-control <?php echo $errors->has('partner') ? 'input-error' : '';?>" name="promotion">
                    <option value="{{ \App\Model\Product::UNPROMOTION }}" {{old('promotion') == \App\Model\Product::UNPROMOTION ?'selected':''}}>Không giảm giá</option>
                    <option value="{{ \App\Model\Product::PROMOTION }}"  {{old('promotion') == \App\Model\Product::PROMOTION ?'selected':''}}>Giảm giá</option>
            </select>
        </div>
        <div class="form-group">
            <label for="name">Giá giảm :</label>
            <input type="text" class="form-control <?php echo $errors->has('sale_off') ? 'input-error' : '';?>" value="{{old('sale_off')}}" name="sale_off" id="sale_off">
        </div>
        <div class="form-group">
            <label for="name">Hình ảnh : <span class="color-red">*</span></label>
            <input type="file" class="form-control <?php echo $errors->has('image') ? 'input-error' : '';?>" value="{{old('image')}}" name="image" id="image">
        </div>
        <div class="form-group">
            <label for="name">Tóm tắt: <span class="color-red">*</span></label>
            <textarea name="summary" class="form-control <?php echo $errors->has('summary') ? 'input-error' : '';?>" rows="5" id="summary"> {{ old('summary')}} </textarea>
        </div>
        <div class="form-group">
            <label for="name">Thể loại của sản phẩm: <span class="color-red">*</span></label>
            <select class="form-control data-select-all <?php echo $errors->has('category_id') ? 'input-error' : '';?>" name="category_id">
                @if(count($categories) > 0)
                    <option value="">{{trans('messages.choose_category')}}</option>
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}" {{old('category_id') == $category->id ?'selected':''}}>{{$category->name}}</option>
                    @endforeach
                @else
                    <option value="">Không có thể loại nào</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label for="name">Tags: <span class="color-red">*</span></label>
            <select class="form-control data-select-all <?php echo $errors->has('tag_id') ? 'input-error' : '';?>" name="tag_id">
                @if(count($tags) > 0)
                    <option value="0">{{trans('messages.choose_tag')}}</option>
                    @foreach ($tags as $tag)
                        <option value="{{$tag->id}}" {{old('tag_id') == $tag->id ?'selected':''}}>{{$tag->name}}</option>
                    @endforeach
                @else
                    <option value="">Không có thẻ tag nào</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label for="name">Miêu tả: <span class="color-red">*</span></label>
            <textarea name="desc" id="text" rows="10" cols="80" class="<?php echo $errors->has('desc') ? 'input-error' : '';?>">
                {{old('desc')}}
            </textarea>
        </div>

        <div class="form-group">
            <label for="">Trạng thái: <span class="color-red">*</span></label>
            <select class="form-control <?php echo $errors->has('status') ? 'input-error' : '';?>" name="status">
                    <option value="{{ \App\Model\Product::UNPUBLIC }}" {{old('status') == \App\Model\Product::UNPUBLIC ?'selected':''}}>Chưa sẳn sàng public</option>
                    <option value="{{ \App\Model\Product::PUBLIC }}"  {{old('status') == \App\Model\Product::PUBLIC ?'selected':''}}>Public sản phẩm</option>
            </select>
        </div>

        <button type="submit" class="btn btn-success">Thêm mới</button>
    </form>

@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')

@stop