@extends('adminlte::page')

@section('title', 'quản lý sản phẩm')

@section('content_header')
    <h1 class="inline">Danh sách các sản phẩm</h1>
    <a class="btn btn-info pull-right" href="{{ route('admin.products.add') }}">Thêm thể sản phẩm mới</a>
@stop

@section('content')
    <p>Tìm kiếm</p>
    <div class="container-fluid">
        <div class="row">
        <form action="{{ route('admin.products.index') }}" method="GET">
                <div class="input-group">
                    <input name="products" type="text" class="form-control" placeholder="Search">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordred table-striped table-bordered text-center">
                    <caption>Danh sách</caption>
                    <thead class="panel-title">
                    <tr>
                        <th scope="col">Stt</th>
                        <th scope="col">Tên</th>
                        <th scope="col">Thể loại sản phẩm</th>
                        <th scope="col">Giá</th>
                        <th scope="col">Giá sale</th>
                        <th scope="col">Hình ảnh</th>
                        <th scope="col">Trạng thái</th>
                        <th scope="col">Xem đánh giá</th>
                        <th scope="col">Xóa</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($products as $product)
                        <tr>
                            <th scope="row">{{ $product->id }}</th>
                            <td>
                                <a data-toggle="tooltip" title="Sửa sẩn phẩm !" href="{{ route('admin.products.edit',['slug' => $product->slug]) }}">{{ $product->name}}</a>
                            </td>
                            <td>{{ $product->category->name }}</td>
                            <td>{{ number_format($product->price, 0 , ',', '.')}} vnđ</td>
                            <td>{!! ($product->promotion == App\Model\Product::UNPROMOTION) ? "không giảm giá" : number_format($product->price - $product->sale_off, 0 , ',', '.') . ' vnđ' !!}</td>
                            <td><img src="{{ asset('storage/products/'. $product->image)}}" style="max-width: 200px; height: 100px;-o-object-fit: contain;" class="img-thumbnail" alt="{{ $product->name }}"></td>
                            <td><a 
                            href="javascript:void(0)" 
                            onclick="changeStatus('{{ $product->slug }}');" 
                            data-toggle="tooltip" 
                            title="{{ $product->status == App\Model\Product::PUBLIC ? 'sản phẩm được kích hoạt' : 'sản phẩm chưa được kích hoạt'}}">
                            <i class="{{ ($product->status == App\Model\Product::PUBLIC) ? 'far fa-check-square fa-lg' : 'far fa-eye-slash'}}" 
                            id="{{ $product->slug }}"
                            data-value="{{ $product->status }}"
                            ></i></a></td>
                            <th><a data-toggle="tooltip" title="Xem đánh giá !" href="{{ route('admin.reviews.index', ['id'=> $product->id]) }}"><i class="fas fa-directions"></i></a></th>
                            <td>
                                <form action="{{route('admin.products.delete',['slug' => $product->slug])}}"
                                      class="form-edit formConfirmDeleteCat" method="post">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-danger submitDelete" value="Xóa">
                                </form>
                            </td>
                        </tr>
                         
                    @empty
                        <tr>
                            <td colspan="12" class="text-center">@lang('messages.no_data')</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>

    </div>

@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')
<script src="{{asset('/js/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('js/custom_admin.js') }}"></script>
<script>
    function changeStatus(product) {
        var status = $('i#' + product).attr('data-value');
        $.ajax({
            url: "/admin/products/change_status/" + product,
            data: {status: status},
            type: "GET",    
            success:function(data){
                var status = data.success;
                $("i#" + product).attr('data-value', status);
                if(status == 0) {
                    $('i#' + product).removeClass('far fa-check-square fa-lg').addClass('far fa-eye-slash');
                    $('i#' + product).parents().attr('title','Sản phẩm không được kích hoạt !');
                }else {
                    $('i#' + product).removeClass('far fa-eye-slash').addClass('far fa-check-square fa-lg');
                    $('i#' + product).parents().attr('title','Sản phẩm được kích hoạt !');
                }
            },
            error: function(err){
                alert('Đã có lỗi xảy ra, vui lòng tải lại trang !');
            }
        });
    }
</script>
@stop