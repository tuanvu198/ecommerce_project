@extends('adminlte::page')

@section('title', 'Thêm người dùng')

@section('content_header')
    <h1>Thêm người dùng</h1>
@stop

@section('content')

    <form action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="name">Tên: <span class="color-red">*</span></label>
            <input type="text" class="form-control <?php echo $errors->has('name') ? 'input-error' : '';?>" value="{{old('name')}}" name="name" id="name">
        </div>
        <div class="form-group">
            <label for="name">Email: <span class="color-red">*</span></label>
            <input type="text" class="form-control <?php echo $errors->has('email') ? 'input-error' : '';?>" value="{{old('email')}}" name="email" id="email">
        </div>
        <div class="form-group">
            <label for="name">Hình ảnh : <span class="color-red">*</span></label>
            <input type="file" class="form-control <?php echo $errors->has('image') ? 'input-error' : '';?>" value="{{old('image')}}" name="image" id="image">
        </div>
        <div class="form-group">
            <label for="password">Nhập mật khẩu</label>
            <input type="password" class="form-control <?php echo $errors->has('password') ? 'input-error' : '';?>" id="password" name="password" placeholder="Nhập mật khẩu">
        </div>

        <div class="form-group">
            <label for="password-confirm">Nhập mật khẩu</label>
            <input type="password" class="form-control <?php echo $errors->has('password_confirmation') ? 'input-error' : '';?>" id="password-confirm" name="password_confirmation" placeholder="Nhập lại mật khẩu">
        </div>
        <div class="form-group">
            <div class="form-group <?php echo $errors->has('sex') ? 'input-error' : '';?>">
                <label for="role">Giới tính</label>
                <select class="form-control" id="sex" name="sex">
                    <option value="{{\App\Model\User::MALE}}" {{\App\Model\User::MALE == old('sex') ? 'selected' : ''}}>Nam</option>
                    <option value="{{\App\Model\User::FEMALE}}" {{\App\Model\User::FEMALE == old('sex') ? 'selected' : ''}}>Nữ</option>
                </select>
            </div>
        </div>

        <button type="submit" class="btn btn-success">Thêm mới</button>
    </form>

@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')

@stop