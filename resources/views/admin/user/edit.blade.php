@extends('adminlte::page')

@section('title', 'Sửa người dùng')

@section('content_header')
    <h1 style="display: inline-block;">Sửa người dùng</h1>
    <a href="{{ route('admin.users.edit.password', ['id' => $user->id ]) }}" class="pull-right btn btn-danger">Thay đổi password</a>
@stop

@section('content')
    <form action="{{ route('admin.users.update', ['id' => $user->id]) }}" method="POST" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="name">Tên: <span class="color-red">*</span></label>
            <input type="text" class="form-control" value="{{ $user->name}}" name="name" id="name">
        </div>
        <div class="form-group">
            <label for="email">Địa chỉ email</label>
            <input type="email" disabled class="form-control" id="email" value="{{$user->email}}" name="email"
                   placeholder="Nhập địa chỉ email">
        </div>
        <div class="form-group">
            <label for="name">Hình ảnh : <span class="color-red">*</span></label>
            <input type="file" class="form-control <?php echo $errors->has('image') ? 'input-error' : '';?>"
                   value="{{old('image')}}" name="image" id="image">
            <p><img src="{{ asset('storage/users/'. $user->image)}}" class="img-thumbnail" alt="{{ $user->name }}"
                    style="max-width: 200px; height: 100px;-o-object-fit: contain;"></p>
        </div>
        <div class="form-group">
            <div class="form-group">
                <label for="role">Giới tính</label>
                <select class="form-control" id="sex" name="sex">
                    <option value="{{\App\Model\User::MALE}}" {{\App\Model\User::MALE == $user->sex ? 'selected' : ''}}>
                        Nam
                    </option>
                    <option value="{{\App\Model\User::FEMALE}}" {{\App\Model\User::FEMALE == $user->sex ? 'selected' : ''}}>
                        Nữ
                    </option>
                </select>
            </div>
        </div>

        <button type="submit" class="btn btn-success">Cập nhật</button>
    </form>
@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')

@stop