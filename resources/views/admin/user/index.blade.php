@extends('adminlte::page')

@section('title', 'quản lý người dùng')

@section('content_header')
    <h1 class="inline">Danh sách người dùng</h1>
    <a class="btn btn-info pull-right" href="{{ route('admin.users.add') }}">Thêm người dùng mới</a>
@stop

@section('content')
    <p>Tìm kiếm</p>
    <div class="container-fluid">
        <div class="row">
            <form action="{{ route('admin.users.index') }}" method="GET">
                <div class="input-group">
                    <input name="categories" type="text" class="form-control" placeholder="Search">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordred table-striped table-bordered text-center">
                    <caption>Danh sách</caption>
                    <thead class="panel-title">
                    <tr>
                        <th scope="col">Stt</th>
                        <th scope="col">Tên</th>
                        <th scope="col">Email</th>
                        <th scope="col">Sửa</th>
                        <th scope="col">Xóa</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($users as $user)
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->name }}</td>
                            <td>
                                {{ $user->email }}
                            </td>
                            <td>
                                <a href="{{ route('admin.users.edit',['id' => $user->id]) }}"
                                   class="btn btn-info"><i class="fas fa-edit">Sửa</i></a>
                            </td>
                            <td>
                                @if(Auth::id() == $user->id)
                                    <button class="btn btn-dark" disabled>Xóa</button>
                                @else
                                    <form action="{{route('admin.users.delete',['id' => $user->id])}}"
                                          class="form-edit formConfirmDeleteCat" method="post">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <input type="submit" class="btn btn-danger submitDelete" value="Xóa">
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="12" class="text-center">@lang('messages.no_data')</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div>

    </div>

@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')
    <script src="{{asset('/js/ckeditor/ckeditor.js')}}"></script>
@stop