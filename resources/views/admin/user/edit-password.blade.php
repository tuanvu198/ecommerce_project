@extends('adminlte::page')

@section('title', 'Đổi mật khẩu')

@section('content_header')
    <h1>Sửa mật khẩu</h1>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tài khoản</h4>
                    <form class="forms-sample" method="post" action="{{route('admin.users.update.password',['id' => $user->id])}}" enctype="multipart/form-data" id="edit_user">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="email">Địa chỉ email</label>
                            <input type="email" disabled class="form-control" id="email" value="{{$user->email }}" name="email">
                        </div>
                        <div class="form-group">
                            <label for="new_password">Nhập mật khẩu cũ</label>
                            <input type="password" class="form-control" id="old_password" value="{{ old('old_password') }}"  name="old_password" placeholder="Nhập mật khẩu cũ" >
                        </div>
                        <div class="form-group">
                            <label for="new_password">Nhập mật khẩu mới</label>
                            <input type="password" class="form-control" id="new_password" value="{{ old('new_password') }}" name="new_password" placeholder="Nhập mật khẩu mới" >
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">Nhập lại mật khẩu</label>
                            <input type="password" class="form-control" id="confirm_password" value="{{ old('new_password_confirmation') }}" name="new_password_confirmation" placeholder="Nhập lại mật khẩu mới" >
                        </div>

                        <div class="form-group">
                            <label for="role">Hình ảnh</label>
                            <img src="{{asset('storage/users/'.(isset($user->image) ? $user->image : ''))}}" alt="{{isset($user->name) ? $user->name : ''}}" class="img img-responsive img-avatar" style="max-width: 300px;margin-bottom: 20px;">
                        </div>
                        <button type="submit" class="btn btn-success mr-2" >Chỉnh sửa</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
