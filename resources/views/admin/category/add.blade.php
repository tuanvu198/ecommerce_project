@extends('adminlte::page')

@section('title', 'Thêm thể loại mới')

@section('content_header')
    <h1>Thêm thể loại</h1>
@stop

@section('content')

    <form action="{{ route('admin.categories.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="name">Tên: <span class="color-red">*</span></label>
            <input type="text" class="form-control" value="{{old('name')}}" name="name" id="name">
        </div>
        <div class="form-group">
            <label for="name">Hình ảnh : <span class="color-red">*</span></label>
            <input type="file" class="form-control <?php echo $errors->has('image') ? 'input-error' : '';?>" value="{{old('image')}}" name="image" id="image">
        </div>
        <div class="form-group">
            <textarea name="desc" id="text" rows="10" cols="80">
                {{old('desc')}}
            </textarea>
        </div>

        <button type="submit" class="btn btn-success">Thêm mới</button>
    </form>

@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')

@stop