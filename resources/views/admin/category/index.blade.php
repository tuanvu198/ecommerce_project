@extends('adminlte::page')

@section('title', 'quản lý thể loại')

@section('content_header')
    <h1 class="inline">Danh sách các thể loại</h1>
    <a class="btn btn-info pull-right" href="{{ route('admin.categories.add') }}">Thêm thể loại mới</a>
@stop

@section('content')
    <p>Tìm kiếm</p>
    <div class="container-fluid">
        <div class="row">
        <form action="{{ route('admin.categories.index') }}" method="GET">
                <div class="input-group">
                    <input name="categories" type="text" class="form-control" placeholder="Search">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordred table-striped table-bordered">
                    <caption>Danh sách</caption>
                    <thead class="panel-title">
                    <tr>
                        <th scope="col">Stt</th>
                        <th scope="col">Tên</th>
                        <th scope="col">Sửa</th>
                        <th scope="col">Xóa</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($categories as $category)
                        <tr>
                            <th scope="row">{{ $category->id }}</th>
                            <td>{{ $category->name }}</td>
                            <td>
                                <a href="{{ route('admin.categories.edit',['slug' => $category->slug]) }}"
                                   class="btn btn-info"><i class="fas fa-edit">Sửa</i></a>
                            </td>
                            <td>
                                <form action="{{route('admin.categories.delete',['slug' => $category->slug])}}"
                                      class="form-edit formConfirmDeleteCat" method="post">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-danger submitDelete" value="Xóa">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="12" class="text-center">@lang('messages.no_data')</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div>

    </div>

@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')
    <script src="{{asset('/js/ckeditor/ckeditor.js')}}"></script>
@stop