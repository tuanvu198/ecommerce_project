@extends('adminlte::page')

@section('title', 'Sửa thể loại')

@section('content_header')
    <h1>Sửa thể loại</h1>
@stop

@section('content')
    <form action="{{ route('admin.categories.update', ['slug' => $category->slug]) }}" method="POST" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="name">Tên: <span class="color-red">*</span></label>
            <input type="text" value=" {{ $category->name  }} " class="form-control" name="name" id="name">
        </div>
        <div class="form-group">
            <label for="name">Hình ảnh : <span class="color-red">*</span></label>
            <input type="file" class="form-control <?php echo $errors->has('image') ? 'input-error' : '';?>" value="{{old('image')}}" name="image" id="image">
            <p><img src="{{ asset('storage/categories/'. $category->image)}}" class="img-thumbnail" alt="{{ $category->name }}" style="max-width: 200px; height: 100px;-o-object-fit: contain;"></p>
        </div>
        <div class="form-group">
            <textarea name="desc" id="text" rows="10" cols="80">
                {{ $category->desc  }}
            </textarea>
        </div>

        <button type="submit" class="btn btn-success">Cập nhật</button>
    </form>
@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop

@section('js')

@stop