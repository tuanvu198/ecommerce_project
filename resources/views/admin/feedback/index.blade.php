@extends('adminlte::page')

@section('title', 'phản hồi từ khách hàng')

@section('content_header')
<h1 class="inline">Danh sách phản hồi - <span class="color-red">{{ $news->name }}</span></h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordred table-striped table-bordered text-center">
                    <caption>Danh sách</caption>
                    <thead class="panel-title">
                    <tr>
                        <th scope="col">Stt</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Nội dung</th>
                        <th scope="col">Xóa</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($feedbacks as $fb)
                        <tr>
                            <th scope="row">{{ $fb->id }}</th>
                            <td>{{ $fb->name }}</td>
                            <td>{{ $fb->email }}</td>
                            <td>
                                {{ $fb->content }}
                            </td>
                            <td>
                                <form action="{{route('admin.feedbacks.delete',['id' => $fb->id])}}"
                                      class="form-edit formConfirmDeleteCat" method="post">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-danger submitDelete" value="Xóa">
                                </form>
                            </td>
                        </tr>
                         
                    @empty
                        <tr>
                            <td colspan="12" class="text-center">@lang('messages.no_data')</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        <a class="btn btn-info" href="{{ route('admin.news.index') }}"><i class="fas fa-backward"></i>&nbsp;&nbsp;Back</a>
        </div>
    </div>
    <!-- Modal -->
    <div id="modalContent" class="modal fade" role="dialog">
    </div>
@stop

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">
    <style>
         .animated {
            -webkit-transition: height 0.2s;
            -moz-transition: height 0.2s;
            transition: height 0.2s;
        }
        .stars
        {
            margin: 20px 0;
            font-size: 24px;
            color: #d17581;
        }
    </style>

@stop

@section('js')
<script src="{{asset('/js/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('js/custom_admin.js') }}"></script>
@stop