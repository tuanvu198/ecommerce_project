<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddProjectUnderConstructionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:4|max:255|unique:projects_under_construction,name',
            'image' => 'required|file|image|max:20480|mimes:jpeg,jpg,png,gif',
            'summary' => 'required|string|min:20|max:1300',
            'desc' => "required|string|min:20|max:15000",
        ];
    }
}
