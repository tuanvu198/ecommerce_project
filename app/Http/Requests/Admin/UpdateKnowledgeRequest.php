<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateKnowledgeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => "required|string|min:5|max:255|". Rule::unique('knowledges')->ignore($this->route('knowledge')),
            'desc' => "required|string|min:20|max:2000",
            'image' => 'nullable|file|image|max:20480|mimes:jpeg,jpg,png,gif',
        ];
    }
}
