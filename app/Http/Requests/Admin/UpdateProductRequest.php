<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:4|max:255|'. Rule::unique('products')->ignore($this->route('product')),
            'price' => "required|integer",
            'promotion' => "required|numeric|min:0|max:1",
            'image' => 'nullable|file|image|max:20480|mimes:jpeg,jpg,png,gif',
            'status' => 'min:0|max:1',
            'summary' => 'required|string|min:20|max:1300',
            'desc' => "required|string|min:20|max:15000",
            'category_id' => 'required|numeric|exists:categories,id',
            'tag_id' => 'nullable|numeric|exists:tags,id',
            'sale_off' => 'nullable|numeric',
        ];
    }
}
