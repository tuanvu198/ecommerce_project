<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class SortProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|numeric|min:0|max:4',
            'sort_price_start' => 'nullable|numeric|min:0|max:200000000',
            'sort_price_end' => 'nullable|numeric|min:0|max:200000000',
        ];
    }
}
