<?php

namespace App\Http\Controllers;

use App\Model\Knowledge;
use App\Model\News;
use App\Repositories\Contract\NewsRepository;

class NewsController extends Controller
{
    private $newspaper;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->newspaper = app(NewsRepository::class);
    }

    public function detail(News $news)
    {
        return view('frontend.news-detail', compact(['news']));
    }
}
