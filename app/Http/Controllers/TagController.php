<?php

namespace App\Http\Controllers;

use App\Http\Requests\Frontend\SortProductRequest;
use App\Model\Tag;
use App\Repositories\Contract\CategoryRepository;
use App\Repositories\Contract\ProductRepository;
use App\Repositories\Contract\TagRepository;
use App\Model\Product;

class TagController extends Controller
{
    private $categoryRepository;
    private $productRepository;
    private $tagRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->categoryRepository = app(CategoryRepository::class);
        $this->productRepository = app(ProductRepository::class);
        $this->tagRepository = app(TagRepository::class);
    }

    public function detail(Tag $tag, SortProductRequest $request)
    {
        $products = $this->productRepository->allWithBuilder()->where('tag_id', $tag->id)->where('status',Product::PUBLIC);
        
        if(isset($request->sort) && $request->has('sort')){
            $sortValue = $this->productRepository->sortUpdated_at($request->sort);
            $products = $products->orderBy('updated_at', $sortValue);
        }

        if(isset($request->sort_price_start) && isset($request->sort_price_end)) {
            $products = $products->whereBetween('price', [$request->sort_price_start, $request->sort_price_end]);
        }

        $products = $products->paginate(12);

        $categories = $this->categoryRepository->all();
        $tags = $this->tagRepository->allWithBuilder()->where('id','<>', $tag->id)->with('products')->get();
        return view('frontend.product-tag', compact(['products', 'tags', 'categories', 'tag']));
    }
}
