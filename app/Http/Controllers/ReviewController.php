<?php

namespace App\Http\Controllers;

use App\Http\Requests\Frontend\AddReviewRequest;
use App\Repositories\Contract\ReviewRepository;
use Illuminate\Http\Request;


class ReviewController extends Controller
{
    private $reviewRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->reviewRepository = app(ReviewRepository::class);
    }

    public function store(AddReviewRequest $request)
    {
        $params = $request->all();
        $params['status'] = 0;
        $this->reviewRepository->create($params);
        return redirect()->back()->with('alert-success', trans('messages.success_created'));
    }
}
