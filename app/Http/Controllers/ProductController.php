<?php

namespace App\Http\Controllers;

use App\Model\Product;
use App\Repositories\Contract\CategoryRepository;
use App\Repositories\Contract\ProductRepository;
use App\Repositories\Contract\TagRepository;

class ProductController extends Controller
{
    private $categoryRepository;
    private $productRepository;
    private $tagRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->categoryRepository = app(CategoryRepository::class);
        $this->productRepository = app(ProductRepository::class);
        $this->tagRepository = app(TagRepository::class);
    }

    public function detail(Product $product)
    {
        $list_products = $this->productRepository->allWithBuilder()->where([['category_id', $product->category_id], ['id','<>', $product->id]])->with('reviews')->get();
        $categories = $this->categoryRepository->allWithBuilder()->where('id', '<>', $product->category_id)->get();
        $tags = $this->tagRepository->all(['products']);
        return view('frontend.product-detail', compact(['product', 'list_products', 'categories', 'tags']));
    }
}
