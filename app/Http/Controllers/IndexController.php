<?php

namespace App\Http\Controllers;

use App\Repositories\Contract\CategoryRepository;
use App\Repositories\Contract\KnowledgeRepository;
use App\Repositories\Contract\NewsRepository;
use App\Repositories\Contract\ProjectUnderConstructionRepository;

class IndexController extends Controller
{
    private $categoryRepository;
    private $projectBuildingRepository;
    private $knowledgeRepository;
    private $newsRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->categoryRepository = app(CategoryRepository::class);
        $this->projectBuildingRepository = app(ProjectUnderConstructionRepository::class);
        $this->knowledgeRepository = app(KnowledgeRepository::class);
        $this->newsRepository = app(NewsRepository::class);
    }

    public function index()
    {
        $categories = $this->categoryRepository->all(['products']);
        $projectsUnderConstruction = $this->projectBuildingRepository->allWithBuilder()->take(DEFAULT_GET_ELEMENT)->get();
        $knowledge = $this->knowledgeRepository->allWithBuilder()->inRandomOrder()->first();
        $news = $this->newsRepository->allWithBuilder()->take(DEFAULT_GET_ELEMENT)->get();

        return view('frontend.index', compact(['categories', 'projectsUnderConstruction', 'knowledge', 'news']));
    }

    public function introduction()
    {
        return view('frontend.introduction');
    }

    public function policyRegulation ()
    {
        return view('frontend.policy-regulation');
    }
}
