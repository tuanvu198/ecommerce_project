<?php

namespace App\Http\Controllers;

use App\Http\Requests\Frontend\SortProductRequest;
use App\Model\Category;
use App\Model\Product;
use App\Repositories\Contract\CategoryRepository;
use App\Repositories\Contract\ProductRepository;
use App\Services\ProductService;
use Illuminate\Http\Request;
use App\Repositories\Contract\TagRepository;

class CategoryController extends Controller
{
    private $categoryRepository;
    private $productRepository;
    private $tagRepository;
    private $productService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->categoryRepository = app(CategoryRepository::class);
        $this->productRepository = app(ProductRepository::class);
        $this->tagRepository = app(TagRepository::class);
        $this->productService = app(ProductService::class);
    }

    public function detail(Category $category, SortProductRequest $request)
    {
        $products = $this->productRepository->allWithBuilder()->where('category_id', $category->id)->where('status',Product::PUBLIC);
        
        if(isset($request->sort) && $request->has('sort')) {
            $sortValue = $this->productService->getSortValueOrderBy($request->sort);
            $products = $products->orderBy('updated_at', $sortValue);
        }
        
        if(isset($request->sort_price_start) && isset($request->sort_price_end)) {
            $products = $products->whereBetween('price', [$request->sort_price_start, $request->sort_price_end]);
        }

        $products = $products->paginate(12);
        $tags = $this->tagRepository->all();
        $categories = $this->categoryRepository->allWithBuilder()->where('id', '<>', $category->id)->get();

        return view('frontend.product', compact(['category', 'products', 'tags', 'categories']));
    }
}
