<?php

namespace App\Http\Controllers;

use App\Model\Knowledge;
use App\Repositories\Contract\NewsRepository;

class KnowledgeController extends Controller
{
    private $newspaper;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->newspaper = app(NewsRepository::class);
    }

    public function detail(Knowledge $knowledge)
    {
        $newspapers = $this->newspaper->allWithBuilder()->where('knowledge_id', $knowledge->id)->paginate(9);
        return view('frontend.news', compact(['knowledge', 'newspapers']));
    }
}
