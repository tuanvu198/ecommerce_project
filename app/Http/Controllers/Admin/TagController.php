<?php

namespace App\Http\Controllers\Admin;

use App\Model\Tag;
use App\Repositories\Contract\TagRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddTagRequest;
use App\Http\Requests\Admin\UpdateTagRequest;
use Illuminate\Http\Request;

class TagController extends Controller
{
    private $tagRepository;

    public function __construct()
    {
    	$this->tagRepository = app(TagRepository::class);
    }

    public function index(Request $request)
    {   
        if (isset($request->tags) && $request->has('tags'))
        {
            $tags = $this->tagRepository->findByName($request->tags);
        }else {
            $tags = $this->tagRepository->all();
        }
        return view('admin.tag.index', compact(['tags']));
    }

    public function add()
    {
        return view('admin.tag.add');
    }

    public function store(AddTagRequest $request)
    {
        $params = $request->all();
        $params['slug'] = str_slug($request->name);

        $this->tagRepository->create($params);

        return redirect()->back()->with('alert-success', trans('messages.success_created'));
    }

    public function detail(Tag $tag)
    {
        return view('admin.tag.edit', compact('tag'));
    }

    public function update(UpdateTagRequest $request, Tag $tag)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        $this->tagRepository->update($tag, $data);

        return redirect()->route('admin.tags.index')->with('alert-success', trans('messages.success_updated'));
    }

    public function delete(Tag $tag)
    {
        $this->tagRepository->destroy($tag);
        return redirect()->back()->with('alert-success', trans('messages.success_deleted'));
    }
}
