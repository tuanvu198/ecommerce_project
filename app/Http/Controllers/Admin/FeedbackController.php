<?php

namespace App\Http\Controllers\Admin;

use App\Model\Feedback;
use App\Repositories\Contract\FeedbackRepository;
use App\Http\Controllers\Controller;
use App\Repositories\Contract\NewsRepository;

class FeedbackController extends Controller
{
    private $feedbackRepository;
    private $newsRepository;

    public function __construct()
    {
        $this->feedbackRepository = app(FeedbackRepository::class);
        $this->newsRepository = app(NewsRepository::class);
    }

    public function index($news_id)
    {
        $feedbacks = $this->feedbackRepository->findByField($news_id, 'news_id', ['news']);
        $news = $this->newsRepository->find($news_id);
        return view('admin.feedback.index', compact(['feedbacks', 'news']));
    }

    public function delete(Feedback $feedback)
    {
        $this->feedbackRepository->destroy($feedback);
        return redirect()->back()->with('alert-success', trans('messages.success_deleted'));
    }

}
