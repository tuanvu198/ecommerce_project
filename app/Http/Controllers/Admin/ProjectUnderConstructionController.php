<?php

namespace App\Http\Controllers\Admin;

use App\Model\ProjectUnderConstruction;
use App\Repositories\Contract\ProjectUnderConstructionRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\AddProjectUnderConstructionRequest;
use App\Services\Helper;
use App\Services\Storages\LocalStorage;
use App\Services\Storages\StorageInterface;
use App\Http\Requests\Admin\UpdateProjectUnderConstructionRequest;

class ProjectUnderConstructionController extends Controller
{
    private $projectUnderConstructionRepository;
    private $helperService;

    public function __construct()
    {
         $this->projectUnderConstructionRepository = app(ProjectUnderConstructionRepository::class);
         $this->helperService = app(Helper::class);
    }

    public function index(Request $request)
    {
    	if (isset($request->project_under_construction) && $request->has('project_under_construction')) {
    		$projects_under_construction = $this->projectUnderConstructionRepository->findByName($request->projectUnderConstructionRepository);
    	}else {
    		$projects_under_construction = $this->projectUnderConstructionRepository->paginate();
    	}

    	return view('admin.project_building.index', compact('projects_under_construction'));
    }

    public function add()
    {
        return view('admin.project_building.add');
    }

    public function store(AddProjectUnderConstructionRequest $request)
    {
        $file = $request->file('image');
        $name = $this->createFileName($file);
        $this->uploadImage($file, $name, 'public/projects_under_construction', new LocalStorage);

        $params = $request->all();
        $params['slug'] = str_slug($request->name);
        $params['image'] = $name;

        $this->projectUnderConstructionRepository->create($params);

        return redirect()->back()->with('alert-success', trans('messages.success_created'));
    }

    public function detail(ProjectUnderConstruction $project_building)
    {
        return view('admin.project_building.edit', compact('project_building'));
    }

    public function update(UpdateProjectUnderConstructionRequest $request, ProjectUnderConstruction $project_building)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        if ($request->hasFile('image')) {
            $name = $this->createFileName($request->image);
            $this->uploadImage($request->image, $name, 'public/projects_under_construction', new LocalStorage);
            $data['image'] = $name;
            @unlink('storage/projects_under_construction/'. $project_building->image);
        }

        $this->projectUnderConstructionRepository->update($project_building, $data);

        return redirect()->route('admin.projects_under_construction.index')->with('alert-success', trans('messages.success_updated'));
    }

    public function delete(ProjectUnderConstruction $project_building)
    {
        $this->projectUnderConstructionRepository->destroy($project_building);
        @unlink('storage/projects_under_construction/'. $project_building->image);
        return redirect()->back()->with('alert-success', trans('messages.success_deleted'));
    }

    /**
     * Upload fileproduct
     * @param $file
     * @param $name
     * @param $path
     * @param StorageInterface $storage
     * @return mixed
     */
    private function uploadImage($file, $name, $path, StorageInterface $storage)
    {
        return $storage->saveAs($file, $name, $path);
    }

    private function createFileName($file)
    {
        $fileEtx = $file->getClientOriginalExtension();
        return $this->helperService->generateStringUUID().'.'.$fileEtx;
    }
}
