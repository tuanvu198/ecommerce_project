<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddNewsRequest;
use App\Http\Requests\Admin\UpdateNewsRequest;
use App\Model\News;
use App\Repositories\Contract\KnowledgeRepository;
use App\Repositories\Contract\NewsRepository;
use App\Services\Helper;
use App\Services\Storages\LocalStorage;
use App\Services\Storages\StorageInterface;

class NewsController extends Controller
{
    private $newsRepository;
    private $knowledgeRepository;
    private $helperService;

    public function __construct()
    {
         $this->newsRepository = app(NewsRepository::class);
         $this->knowledgeRepository = app(KnowledgeRepository::class);
         $this->helperService = app(Helper::class);
    }

    public function index(Request $request)
    {
    	if (isset($request->news) && $request->has('news')) {
    		$news = $this->newsRepository->findByName($request->news);
    	}else {
    		$news = $this->newsRepository->paginate();
    	}

    	return view('admin.news.index', compact('news'));
    }

    public function add()
    {
        $knowledges = $this->knowledgeRepository->all();
        return view('admin.news.add', compact('knowledges'));
    }

    public function store(AddNewsRequest $request)
    {
        $file = $request->file('image');
        $name = $this->createFileName($file);
        $this->uploadImage($file, $name, 'public/news', new LocalStorage);

        $params = $request->all();
        $params['slug'] = str_slug($request->name);
        $params['image'] = $name;

        $this->newsRepository->create($params);

        return redirect()->back()->with('alert-success', trans('messages.success_created'));
    }

    public function detail(News $newspaper)
    {
        $knowledges = $this->knowledgeRepository->all();
        return view('admin.news.edit', compact(['newspaper', 'knowledges']));
    }

    public function update(UpdateNewsRequest $request, News $newspaper)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        if ($request->hasFile('image')) {
            $name = $this->createFileName($request->image);
            $this->uploadImage($request->image, $name, 'public/news', new LocalStorage);
            $data['image'] = $name;
            @unlink('storage/news/'. $newspaper->image);
        }

        $this->newsRepository->update($newspaper, $data);

        return redirect()->route('admin.news.index')->with('alert-success', trans('messages.success_updated'));
    }

    public function delete(News $newspaper)
    {
        @unlink('storage/news/'. $newspaper->image);
        $this->newsRepository->destroy($newspaper);
        return redirect()->back()->with('alert-success', trans('messages.success_deleted'));
    }

    /**
     * Upload fileproduct
     * @param $file
     * @param $name
     * @param $path
     * @param StorageInterface $storage
     * @return mixed
     */
    private function uploadImage($file, $name, $path, StorageInterface $storage)
    {
        return $storage->saveAs($file, $name, $path);
    }

    private function createFileName($file)
    {
        $fileEtx = $file->getClientOriginalExtension();
        return $this->helperService->generateStringUUID().'.'.$fileEtx;
    }
}
