<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddUserRequest;
use App\Http\Requests\Admin\UpdateUserPasswordRequest;
use App\Http\Requests\Admin\UpdateUserRequest;
use App\Repositories\Contract\UserRepository;
use App\Services\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\Storages\LocalStorage;
use App\Services\Storages\StorageInterface;
use App\Model\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $userRepository;
    private $helperService;

    public function __construct()
    {
        $this->userRepository = app(UserRepository::class);
        $this->helperService = app(Helper::class);
    }

    public function index(Request $request)
    {
        if (isset($request->users) && $request->has('users')) {
            $users = $this->userRepository->findByName($request->users);
        } else {
            $users = $this->userRepository->paginate();
        }

        return view('admin.user.index', compact(['users']));
    }

    public function add()
    {
        return view('admin.user.add');
    }

    public function store(AddUserRequest $request)
    {
        $file = $request->file('image');
        $name = $this->createFileName($file);
        $this->uploadImage($file, $name, 'public/users', new LocalStorage);

        $data = $request->all();
        $data['token_active'] = generateToken();
        $data['password'] = bcrypt($data['password']);
        $data['image'] = $name;

        $this->userRepository->create($data);
        return redirect()->back()->with('alert-success', trans('messages.success_created'));
    }

    public function detail(User $user)
    {
        return view('admin.user.edit', compact('user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $data = $request->all();
        $data['email'] = $user->email;

        if ($request->hasFile('image')) {
            $name = $this->createFileName($request->image);
            $this->uploadImage($request->image, $name, 'public/users', new LocalStorage);
            $data['image'] = $name;
            @unlink('storage/users/' . $user->image);
            $data['image'] = $name;
        }

        $this->userRepository->update($user, $data);

        return redirect()->route('admin.users.edit', ['id' => $user->id])->with('alert-success', trans('messages.success_updated'));
    }

    public function editPassword(User $user)
    {
        return view('admin.user.edit-password', compact('user'));
    }

    public function updatePassword(UpdateUserPasswordRequest $request, User $user)
    {
        $current_password = Auth::user()->password;
        if(!Hash::check($request->old_password, $current_password)){
            return redirect()->back()->with('alert-error', trans('messages.updated_password_fail'));
        }

        $this->userRepository->update($user, ['password' => bcrypt($request->input('new_password'))]);

        if (auth()->user()->email == $user->email) {
            auth()->logout();
        }
        return redirect()->back()->with('alert-success', trans('messages.updated_success'));
    }

    public function delete(User $user)
    {
        if($user->id == auth()->id()){
            return abort(404);
        }
        $this->userRepository->destroy($user);
        return redirect()->back()->with('alert-success', trans('messages.success_deleted'));
    }

    /**
     * Upload fileproduct
     * @param $file
     * @param $name
     * @param $path
     * @param StorageInterface $storage
     * @return mixed
     */
    private function uploadImage($file, $name, $path, StorageInterface $storage)
    {
        return $storage->saveAs($file, $name, $path);
    }

    private function createFileName($file)
    {
        $fileEtx = $file->getClientOriginalExtension();
        return $this->helperService->generateStringUUID() . '.' . $fileEtx;
    }
}
