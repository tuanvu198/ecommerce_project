<?php

namespace App\Http\Controllers\Admin;

use App\Model\Knowledge;
use App\Repositories\Contract\KnowledgeRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddKnowledgeRequest;
use App\Http\Requests\Admin\UpdateKnowledgeRequest;
use Illuminate\Http\Request;
use App\Services\Storages\LocalStorage;
use App\Services\Helper;
use App\Services\Storages\StorageInterface;

class KnowledgeController extends Controller
{
    protected $knowledgeRepository;
    private $helperService;

    public function __construct()
    {
        $this->knowledgeRepository = app(KnowledgeRepository::class);
        $this->helperService = app(Helper::class);
    }

    public function index(Request $request)
    {   
        if (isset($request->knowledges) && $request->has('knowledges'))
        {
            $knowledges = $this->knowledgeRepository->findByName($request->knowledges);
        }else {
            $knowledges = $this->knowledgeRepository->all();
        }
        return view('admin.knowledge.index', compact(['knowledges']));
    }

    public function add()
    {
        return view('admin.knowledge.add');
    }

    public function store(AddKnowledgeRequest $request)
    {
        $file = $request->file('image');
        $name = $this->createFileName($file);
        $this->uploadImage($file, $name, 'public/knowledges', new LocalStorage);

        $params = $request->all();
        $params['slug'] = str_slug($request->name);
        $params['image'] = $name;

        $this->knowledgeRepository->create($params);

        return redirect()->back()->with('alert-success', trans('messages.success_created'));
    }

    public function detail(Knowledge $knowledge)
    {
        return view('admin.knowledge.edit', compact('knowledge'));
    }

    public function update(UpdateKnowledgeRequest $request, Knowledge $knowledge)
    {
        $params = $request->all();
        $params['slug'] = str_slug($request->name);

        if ($request->hasFile('image')) {
            $name = $this->createFileName($request->image);
            $this->uploadImage($request->image, $name, 'public/knowledges', new LocalStorage);
            $params['image'] = $name;
            @unlink('storage/knowledges/'. $knowledge->image);
        }
        $this->knowledgeRepository->update($knowledge, $params);

        return redirect()->route('admin.knowledges.index')->with('alert-success', trans('messages.success_updated'));
    }

    public function delete(Knowledge $knowledge)
    {
        $knowledge->news()->forceDelete();
        @unlink('storage/knowledges/'. $knowledge->image);
        $this->knowledgeRepository->destroy($knowledge);
        return redirect()->back()->with('alert-success', trans('messages.success_deleted'));
    }

    /**
     * Upload fileproduct
     * @param $file
     * @param $name
     * @param $path
     * @param StorageInterface $storage
     * @return mixed
     */
    private function uploadImage($file, $name, $path, StorageInterface $storage)
    {
        return $storage->saveAs($file, $name, $path);
    }

    private function createFileName($file)
    {
        $fileEtx = $file->getClientOriginalExtension();
        return $this->helperService->generateStringUUID().'.'.$fileEtx;
    }
}
