<?php

namespace App\Http\Controllers\Admin;

use App\Model\Category;
use App\Repositories\Contract\CategoryRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddCategoryRequest;
use App\Http\Requests\Admin\UpdateCategoryRequest;
use Illuminate\Http\Request;
use App\Services\Storages\LocalStorage;
use App\Services\Storages\StorageInterface;
use App\Services\Helper;

class CategoryController extends Controller
{
    private $categoryRepository;
    private $helperService;

    public function __construct()
    {
        $this->categoryRepository = app(CategoryRepository::class);
        $this->helperService = app(Helper::class);
    }

    public function index(Request $request)
    {   
        if (isset($request->categories) && $request->has('categories'))
        {
            $categories = $this->categoryRepository->findByName($request->categories);
        }else {
            $categories = $this->categoryRepository->all();
        }

        return view('admin.category.index', compact(['categories']));
    }

    public function add()
    {
        return view('admin.category.add');
    }

    public function store(AddCategoryRequest $request)
    {
        $file = $request->file('image');
        $name = $this->createFileName($file);
        $this->uploadImage($file, $name, 'public/categories', new LocalStorage);

        $params = $request->all();
        $params['slug'] = str_slug($request->name);
        $params['image'] = $name;

        $this->categoryRepository->create($params);

        return redirect()->back()->with('alert-success', trans('messages.success_created'));
    }

    public function detail(Category $category)
    {
        return view('admin.category.edit', compact('category'));
    }

    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $params = $request->all();
        $params['slug'] = str_slug($request->name);

        if ($request->hasFile('image')) {
            $name = $this->createFileName($request->image);
            $this->uploadImage($request->image, $name, 'public/categories', new LocalStorage);
            $params['image'] = $name;
            @unlink('storage/categories/'. $category->image);
        }

        $this->categoryRepository->update($category, $params);

        return redirect()->route('admin.categories.index')->with('alert-success', trans('messages.success_updated'));
    }

    public function delete(Category $category)
    {
        $category->products()->forceDelete();
        @unlink('storage/categories/'. $category->image);
        $this->categoryRepository->destroy($category);
        return redirect()->back()->with('alert-success', trans('messages.success_deleted'));
    }

    /**
     * Upload fileproduct
     * @param $file
     * @param $name
     * @param $path
     * @param StorageInterface $storage
     * @return mixed
     */
    private function uploadImage($file, $name, $path, StorageInterface $storage)
    {
        return $storage->saveAs($file, $name, $path);
    }

    private function createFileName($file)
    {
        $fileEtx = $file->getClientOriginalExtension();
        return $this->helperService->generateStringUUID().'.'.$fileEtx;
    }
}
