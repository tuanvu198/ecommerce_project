<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contract\ReviewRepository;
use App\Model\Review;
use App\Http\Requests\Admin\ReviewUpdateStatusRequest;
use App\Repositories\Contract\ProductRepository;

class ReviewController extends Controller
{
    private $reviewRepository;
    private $productRepository;

    public function __construct()
    {
        $this->reviewRepository = app(ReviewRepository::class);
        $this->productRepository = app(ProductRepository::class);
    }

    public function index($product_id)
    {
        $ratings_product = $this->reviewRepository->findByField($product_id, 'product_id',['product']);
        $product = $this->productRepository->find($product_id);
        return view('admin.review.index', compact(['ratings_product', 'product']));
    }

    public function detail(Review $review)
    {
        return view('admin.review.modal', compact('review'));
    }

    public function delete(Review $review)
    {
        $this->reviewRepository->destroy($review);
        return redirect()->back()->with('alert-success', trans('messages.success_deleted'));
    }

    public function changeStatus(Review $review, ReviewUpdateStatusRequest $request)
    {
        $data['status'] = ($request->status == Review::PUBLIC ) ? Review::UNPUBLIC : Review::PUBLIC;
        $this->reviewRepository->update($review, $data);

        return response()->json(['success'=> $data['status']]);
    }
}
