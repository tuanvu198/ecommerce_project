<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contract\ProductRepository;
use App\Repositories\Contract\CategoryRepository;
use App\Repositories\Contract\TagRepository;
use App\Services\Helper;
use App\Services\Storages\StorageInterface;
use App\Services\Storages\LocalStorage;
use App\Http\Requests\Admin\AddProductRequest;
use App\Http\Requests\Admin\ProductUpdateStatusRequest;
use App\Http\Requests\Admin\UpdateProductRequest;
use App\Model\Product;

class ProductController extends Controller
{
    private $productRepository;
    private $categoryRepository;
    private $tagRepository;
    private $helperService;

    public function __construct()
    {
        $this->productRepository = app(ProductRepository::class);
        $this->categoryRepository = app(CategoryRepository::class);
        $this->tagRepository = app(TagRepository::class);
        $this->helperService = app(Helper::class);
    }

    public function index(Request $request)
    {   
        if (isset($request->products) && $request->has('products'))
        {
            $products = $this->productRepository->findByName($request->products);
        }else {
            $products = $this->productRepository->paginate();
        }

        return view('admin.product.index', compact('products'));
    }

    public function add()
    {
        $categories = $this->categoryRepository->all();
        $tags = $this->tagRepository->all();

        return view('admin.product.add', compact(['categories', 'tags']));
    }

    public function store(AddproductRequest $request)
    {
        $file = $request->file('image');
        $name = $this->createFileName($file);
        $this->uploadImage($file, $name, 'public/products', new LocalStorage);

        $params = $request->all();
        $params['slug'] = str_slug($request->name);
        $params['image'] = $name;

        $this->productRepository->create($params);

        return redirect()->back()->with('alert-success', trans('messages.success_created'));
    }

    public function detail(Product $product)
    {
        $categories = $this->categoryRepository->all();
        $tags = $this->tagRepository->all();

        return view('admin.product.edit', compact('product', 'tags', 'categories'));
    }

    public function update(UpdateproductRequest $request, Product $product)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        if ($request->hasFile('image')) {
            $name = $this->createFileName($request->image);
            $this->uploadImage($request->image, $name, 'public/products', new LocalStorage);
            $data['image'] = $name;
            @unlink('storage/products/'. $product->image);
        }

        $this->productRepository->update($product, $data);

        return redirect()->route('admin.products.index')->with('alert-success', trans('messages.success_updated'));
    }

    public function delete(Product $product)
    {
        $product->reviews()->forceDelete();
        $this->productRepository->destroy($product);
        @unlink('storage/products/'. $product->image);
        return redirect()->back()->with('alert-success', trans('messages.success_deleted'));
    }

    public function changeStatus(Product $product, ProductUpdateStatusRequest $request)
    {
        $data['status'] = ($request->status == Product::PUBLIC ) ? Product::UNPUBLIC : Product::PUBLIC;
        $this->productRepository->update($product, $data);

        return response()->json(['success'=> $data['status']]);
    }

    /**
     * Upload fileproduct
     * @param $file
     * @param $name
     * @param $path
     * @param StorageInterface $storage
     * @return mixed
     */
    private function uploadImage($file, $name, $path, StorageInterface $storage)
    {
        return $storage->saveAs($file, $name, $path);
    }

    private function createFileName($file)
    {
        $fileEtx = $file->getClientOriginalExtension();
        return $this->helperService->generateStringUUID().'.'.$fileEtx;
    }
}
