<?php

namespace App\Http\Controllers;

use App\Http\Requests\Frontend\AddReviewRequest;
use App\Http\Requests\Frontend\FeedbackRequest;
use App\Repositories\Contract\FeedbackRepository;
use function GuzzleHttp\Promise\all;

class FeedbackController extends Controller
{
    private $feedbackRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->feedbackRepository = app(FeedbackRepository::class);
    }

    public function store(FeedbackRequest $request)
    {
        $this->feedbackRepository->create($request->all());
        return redirect()->back()->with('alert-success', trans('messages.success_created'));
    }
}
