<?php

namespace App\Http\Controllers;

use App\Model\ProjectUnderConstruction;
use App\Repositories\Contract\ProjectUnderConstructionRepository;


class ProjectUnderConstructionController extends Controller
{
    private $projectBuildingRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->projectBuildingRepository = app(ProjectUnderConstructionRepository::class);
    }

    public function index()
    {
        $projectsUnderConstruction = $this->projectBuildingRepository->all();
        return view('frontend.project-building', compact('projectsUnderConstruction'));
    }

    public function detail(ProjectUnderConstruction $projectUnderConstruction)
    {
        return view('frontend.project-building-detail', compact('projectUnderConstruction'));
    }
}
