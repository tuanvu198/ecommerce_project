<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductLabel extends Model
{
    protected $table = 'product_labels';

    protected $fillable = [
    	'name','slug','desc','category_id'
    ];

    public function products()
    {
    	return $this->hasMany('App\Model\Product','product_label_id');
    }

    public function category()
    {
    	return $this->belongsTo('App\Model\Category','category_id');
    }
}
