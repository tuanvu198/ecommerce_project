<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const UNPROMOTION = 0;
    const PROMOTION = 1;
    const UNPUBLIC = 0;
    const PUBLIC = 1;

    const SORT_DEFAUL = 0;
    const SORT_ASC = 1;
    const SORT_DESC = 2;
    const SORT_RATING = 3;

    protected $fillable = [
    	'name','slug','price','desc','summary','image','category_id','tag_id','status', 'promotion', 'sale_off'
    ];

    public function category()
    {
    	return $this->belongsTo('App\Model\Category','category_id');
    }

    public function tag()
    {
    	return $this->belongsTo('App\Model\Tag','tags_id');
    }

    public function product_lable()
    {
    	return $this->belongsTo('App\Model\ProductLabel','product_lable_id');
    }

    public function reviews()
    {
    	return $this->hasMany('App\Model\Review','product_id');
    }
}
