<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedbacks';

    protected $fillable = [
    	'content','name','email','website_address', 'news_id'
    ];

    public function news()
    {
        return $this->belongsTo('App\Model\News','news_id');
    }
}
