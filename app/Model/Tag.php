<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
    	'name','slug'
    ];

    public function products()
    {
    	return $this->hasMany('App\Model\Product','tag_id');
    }
}
