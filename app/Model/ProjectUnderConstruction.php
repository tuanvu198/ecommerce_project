<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProjectUnderConstruction extends Model
{
    protected $table = 'projects_under_construction';

    protected $fillable = [
    	'name','slug','image','summary','desc'
    ];

}
