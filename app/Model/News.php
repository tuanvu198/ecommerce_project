<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
    	'name', 'slug', 'image', 'summary', 'desc', 'knowledge_id'
    ];

    public function knowledge()
    {
    	return $this->belongsTo('App\Model\Knowledge','knowledge_id');
    }

    public function feedbacks()
    {
        return $this->hasMany('App\Model\Feedback', 'news_id');
    }
}
