<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'categories';

    protected $fillable = [
    	'name','slug','desc', 'image'
    ];

    public function products()
    {
    	return $this->hasMany('App\Model\Product','category_id');
    }

    public function product_labels()
    {
    	return $this->hasMany('App\Model\ProductLabel','category_id');
    }
}
