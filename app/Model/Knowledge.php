<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Knowledge extends Model
{
	protected $table = 'knowledges';

    protected $fillable = [
    	'name','slug', 'image', 'desc'
    ];

    public function news()
    {
    	return $this->hasMany('App\Model\News','knowledge_id');
    }
}
