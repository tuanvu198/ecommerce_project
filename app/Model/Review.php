<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    const UNPUBLIC = 0;
    const PUBLIC = 1;
    
    protected $fillable = [
    	'name','email','product_id','rating','content','status'
    ];

    public function product()
    {
    	return $this->belongsTo('App\Model\Product','product_id');
    }

}
