<?php
namespace App\Services\Storages;

use App\Exceptions\UploadFailException;

class LocalStorage implements StorageInterface
{

    /**
     * Upload file with old name
     *
     * @param $file
     * @param string $path
     * @throws UploadFailException
     */
    public function save($file, $path = '')
    {
        try {
            $file->store($path);
        } catch (\Exception $exception) {
            throw new UploadFailException('upload_fail');
        }
    }

    /**
     * Upload change name file
     *
     * @param $file
     * @param $name
     * @param string $path
     * @throws UploadFailException
     */
    public function saveAs($file, $name, $path = '')
    {
        try {
            $file->storeAs($path, $name);
        } catch (\Exception $exception) {
            throw new UploadFailException('upload_fail');
        }
    }
}
