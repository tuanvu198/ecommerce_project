<?php
namespace App\Services\Storages;

interface StorageInterface
{
    public function saveAs($file, $name, $path = '');

    public function save($file, $path = '');
}
