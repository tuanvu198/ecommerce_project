<?php
namespace App\Services;

class Helper
{
    public function generateStringUUID()
    {
        return time().substr(md5(mt_rand()), 0, 7);
    }
}
