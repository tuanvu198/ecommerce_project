<?php
namespace App\Services;

class ProductService
{
    public function getSortValueOrderBy($value)
    {
        $sort = "";
        switch ($value) {
            case '1':
                $sort = 'ASC';
                break;
            case "2":
                $sort = 'DESC';
                break;
            default:
                $sort = 'DESC';
        }
        return $sort;
    }
}
