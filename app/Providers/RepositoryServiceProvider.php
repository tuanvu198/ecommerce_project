<?php

namespace App\Providers;

use App\Repositories\Contract\BaseRepository;
use App\Repositories\Contract\CategoryRepository;
use App\Repositories\Contract\FeedbackRepository;
use App\Repositories\Contract\ProjectUnderConstructionRepository;
use App\Repositories\Contract\TagRepository;
use App\Repositories\Contract\ProductRepository;
use App\Repositories\Contract\KnowledgeRepository;
use App\Repositories\Contract\NewsRepository;
use App\Repositories\Contract\ReviewRepository;
use App\Repositories\Contract\UserRepository;
use App\Repositories\Eloquent\EloquentBaseRepository;
use App\Repositories\Eloquent\EloquentCategoryRepository;
use App\Repositories\Eloquent\EloquentFeedbackRepository;
use App\Repositories\Eloquent\EloquentProjectUnderConstructionRepository;
use App\Repositories\Eloquent\EloquentTagRepository;
use App\Repositories\Eloquent\EloquentProductRepository;
use App\Repositories\Eloquent\EloquentKnowledgeRepository;
use App\Repositories\Eloquent\EloquentNewsRepository;
use App\Repositories\Eloquent\EloquentReviewRepository;
use App\Repositories\Eloquent\EloquentUserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FeedbackRepository::class, EloquentFeedbackRepository::class);
        $this->app->bind(UserRepository::class, EloquentUserRepository::class);
        $this->app->bind(ReviewRepository::class, EloquentReviewRepository::class);
        $this->app->bind(NewsRepository::class, EloquentNewsRepository::class);
        $this->app->bind(ProjectUnderConstructionRepository::class, EloquentProjectUnderConstructionRepository::class);
        $this->app->bind(KnowledgeRepository::class, EloquentKnowledgeRepository::class);
        $this->app->bind(TagRepository::class, EloquentTagRepository::class);
        $this->app->bind(ProductRepository::class, EloquentProductRepository::class);
        $this->app->bind(CategoryRepository::class, EloquentCategoryRepository::class);
        $this->app->bind(BaseRepository::class, EloquentBaseRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
