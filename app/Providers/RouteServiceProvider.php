<?php

namespace App\Providers;

use App\Model\Category;
use App\Model\Feedback;
use App\Model\Product;
use App\Model\ProjectUnderConstruction;
use App\Model\Knowledge;
use App\Model\News;
use App\Model\Review;
use App\Model\Tag;
use App\Model\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        parent::boot();

        Route::pattern('id', '[0-9]+');

        //explicit model binding
        Route::bind('category', function ($value) {
            return Category::where('slug', $value)->first() ?? abort(404);
        });

        Route::bind('knowledge', function ($value) {
            return Knowledge::where('slug', $value)->first() ?? abort(404);
        });

        Route::bind('tag', function ($value) {
            return Tag::where('slug', $value)->first() ?? abort(404);
        });

        Route::bind('product', function ($value) {
            return Product::where('slug', $value)->first() ?? abort(404);
        });
        
        Route::bind('project_building', function ($value) {
            return ProjectUnderConstruction::where('slug', $value)->first() ?? abort(404);
        });

        Route::bind('newspaper', function ($value) {
            return News::where('slug', $value)->first() ?? abort(404);
        });

        Route::bind('review', function ($value) {
            return Review::where('id', $value)->first() ?? abort(404);
        });

        Route::bind('feedback', function ($value) {
            return Feedback::where('id', $value)->first() ?? abort(404);
        });

        Route::bind('user', function ($value) {
            return User::where('id', $value)->first() ?? abort(404);
        });
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }


    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
