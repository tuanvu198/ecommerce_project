<?php
/**
 * Created by PhpStorm.
 * User: vu
 * Date: 18/08/2019
 * Time: 15:30
 */
namespace App\Repositories\Eloquent;

use App\Model\Review;
use App\Repositories\Contract\ReviewRepository;

class EloquentReviewRepository extends EloquentBaseRepository implements ReviewRepository
{
    public function __construct(Review $model)
    {
        parent::__construct($model);
    }
}