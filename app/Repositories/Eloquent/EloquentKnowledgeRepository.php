<?php
/**
 * Created by PhpStorm.
 * User: vu
 * Date: 18/08/2019
 * Time: 15:30
 */
namespace App\Repositories\Eloquent;

use App\Model\Knowledge;
use App\Repositories\Contract\KnowledgeRepository;

class EloquentKnowledgeRepository extends EloquentBaseRepository implements KnowledgeRepository
{
    public function __construct(Knowledge $model)
    {
        parent::__construct($model);
    }
}