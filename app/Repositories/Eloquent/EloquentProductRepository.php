<?php
/**
 * Created by PhpStorm.
 * User: vu
 * Date: 18/08/2019
 * Time: 15:30
 */
namespace App\Repositories\Eloquent;

use App\Model\Product;
use App\Repositories\Contract\ProductRepository;

class EloquentProductRepository extends EloquentBaseRepository implements ProductRepository
{
    public function __construct(Product $model)
    {
        parent::__construct($model);
    }

    public function sortUpdated_at($sortValue)
    {
        $sort = "";
        switch ($sortValue) {
            case '1':
                $sort = 'ASC';
                break;
            case "2":
                $sort = 'DESC';
                break;
            default:
                $sort = 'DESC';
            }
        return $sort;
    }
}