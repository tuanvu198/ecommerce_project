<?php
/**
 * Created by PhpStorm.
 * User: vu
 * Date: 18/08/2019
 * Time: 15:30
 */
namespace App\Repositories\Eloquent;

use App\Model\Tag;
use App\Repositories\Contract\TagRepository;

class EloquentTagRepository extends EloquentBaseRepository implements TagRepository
{
    public function __construct(Tag $model)
    {
        parent::__construct($model);
    }
}