<?php
/**
 * Created by PhpStorm.
 * User: vu
 * Date: 18/08/2019
 * Time: 15:30
 */
namespace App\Repositories\Eloquent;

use App\Model\Feedback;
use App\Repositories\Contract\UserRepository;

class EloquentFeedbackRepository extends EloquentBaseRepository implements UserRepository
{
    public function __construct(Feedback $model)
    {
        parent::__construct($model);
    }
}