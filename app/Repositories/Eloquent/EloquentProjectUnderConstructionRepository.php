<?php
/**
 * Created by PhpStorm.
 * User: vu
 * Date: 18/08/2019
 * Time: 15:30
 */
namespace App\Repositories\Eloquent;

use App\Model\ProjectUnderConstruction;
use App\Repositories\Contract\ProjectUnderConstructionRepository;

class EloquentProjectUnderConstructionRepository extends EloquentBaseRepository implements ProjectUnderConstructionRepository
{
    public function __construct(ProjectUnderConstruction $model)
    {
        parent::__construct($model);
    }
}