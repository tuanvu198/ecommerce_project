<?php
/**
 * Created by PhpStorm.
 * User: vu
 * Date: 18/08/2019
 * Time: 15:30
 */
namespace App\Repositories\Eloquent;

use App\Model\News;
use App\Repositories\Contract\NewsRepository;

class EloquentNewsRepository extends EloquentBaseRepository implements NewsRepository
{
    public function __construct(News $model)
    {
        parent::__construct($model);
    }
}