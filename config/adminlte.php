<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'Admin Led',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>Admin</b>LED',

    'logo_mini' => '<b>A</b>LED',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | light variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'blue',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => '/',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and a URL. You can also specify an icon from Font
    | Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    */

    'menu' => [
//        [
//            'text' => 'search',
//            'search' => false,
//        ],
//        ['header' => 'main_navigation'],
//        [
//            'text' => 'blog',
//            'url'  => 'admin/blog',
//            'can'  => 'manage-blog',
//        ],
        [
            'text'        => 'Xem trang chủ',
            'url'         => '/',
            'icon'        => 'fab fa-rev',
            'label_color' => 'success',
            'target'      => 'blank'
        ],
        ['header' => 'cài đặt chung'],
//        [
//            'text' => 'Thông tin cá nhân',
//            'url'  => 'admin/user/detail/'. \Illuminate\Support\Facades\Auth::id(),
//            'icon' => 'fas fa-fw fa-user',
//        ],
        // [
        //     'text' => 'Thay đổi password',
        //     'url'  => 'admin/',
        //     'icon' => 'fas fa-fw fa-lock',
        // ],
        [
            'text' => 'Quản lý người dùng',
            'icon' => 'fas fa-users',
            'submenu' => [
                [
                    'text' => 'Danh sách người dùng',
                    'url'  => 'admin/users/',
                ],
                [
                    'text' => 'Thêm người dùng',
                    'url'  => 'admin/users/store',
                ],
            ]
        ],
        [
            'text' => 'Quản lý thể loại',
            'icon' => 'fas fa-palette',
            'submenu' => [
                [
                    'text' => 'Danh sách thể loại',
                    'url'  => 'admin/categories/',
                ],
                [
                    'text' => 'Thêm thể loại',
                    'url'  => 'admin/categories/store',
                ],
            ]
        ],
        [
            'text' => 'Quản lý sản phẩm',
            'icon' => 'fas fa-tasks',
            'submenu' => [
                [
                    'text' => 'Danh sách sản phẩm',
                    'url'  => 'admin/products/',
                ],
                [
                    'text' => 'Thêm sản phẩm',
                    'url'  => 'admin/products/store',
                ],
            ]
        ],
        [
            'text' => 'Quản lý kiến thức',
            'icon' => 'fas fa-assistive-listening-systems',
            'submenu' => [
                [
                    'text' => 'Danh sách kiến thức',
                    'url'  => 'admin/knowledges',
                ],
                [
                    'text' => 'Thêm kiến thức',
                    'url'  => 'admin/knowledges/store',
                ],
            ]
        ],
        [
            'text'    => 'Quản lý tin tức',
            'icon'    => 'far fa-newspaper',
            'submenu' => [
                [
                    'text' => 'Danh sách tin tức',
                    'url'  => 'admin/news/',
                ],
                [
                    'text' => 'Thêm tin tức mới',
                    'url'  => 'admin/news/store',
                ],
            ],
        ],
        [
            'text'    => 'Quản lý dự án đang thi công',
            'icon'    => 'fas fa-project-diagram',
            'submenu' => [
                [
                    'text' => 'Danh sách dự án đang thi công',
                    'url'  => 'admin/projects_under_construction',
                ],
                [
                    'text' => 'Thêm mới dự án thi công',
                    'url'  => 'admin/projects_under_construction/store',
                ],
            ],
        ],
        [
            'text' => 'Quản lý tag',
            'icon' => 'fas fa-thumbtack',
            'submenu' => [
                [
                    'text' => 'Danh sách tag',
                    'url'  => 'admin/tags/',
                ],
                [
                    'text' => 'Thêm tag',
                    'url'  => 'admin/tags/store',
                ],
            ]
        ],
//        ['header' => 'labels'],
//        [
//            'text'       => 'important',
//            'icon_color' => 'red',
//        ],
//        [
//            'text'       => 'warning',
//            'icon_color' => 'yellow',
//        ],
//        [
//            'text'       => 'information',
//            'icon_color' => 'aqua',
//        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Configure which JavaScript plugins should be included. At this moment,
    | DataTables, Select2, Chartjs and SweetAlert are added out-of-the-box,
    | including the Javascript and CSS files from a CDN via script and link tag.
    | Plugin Name, active status and files array (even empty) are required.
    | Files, when added, need to have type (js or css), asset (true or false) and location (string).
    | When asset is set to true, the location will be output using asset() function.
    |
    */

    'plugins' => [
        [
            'name' => 'Datatables',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.css',
                ],
            ],
        ],
        [
            'name' => 'Select2',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        [
            'name' => 'Chartjs',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        [
            'name' => 'Sweetalert2',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//unpkg.com/sweetalert/dist/sweetalert.min.js',
                ],
            ],
        ],
        [
            'name' => 'Pace',
            'active' => true,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],
];
