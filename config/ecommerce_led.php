<?php

if (!defined('DEFAULT_PAGINATION_PER_PAGE')) {
    define('DEFAULT_PAGINATION_PER_PAGE', 16);
}

if (!defined('DEFAULT_GET_ELEMENT')) {
    define('DEFAULT_GET_ELEMENT', 8);
}