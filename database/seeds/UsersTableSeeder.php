<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'adminled',
            'email' => 'adminled@gmail.com',
            'password' => bcrypt('123456adminled'),
            'image' => '1.jpg',
            'sex' => 1
        ]);
    }
}
