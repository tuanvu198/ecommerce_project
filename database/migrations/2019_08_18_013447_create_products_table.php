<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug');
            $table->integer('price');
            $table->integer('sale_off');
            $table->text('desc');
            $table->text('summary');
            $table->string('image');
            $table->integer('category_id');
            $table->integer('tags_id')->nullable();
            $table->integer('product_lable_id')->nullable();
            $table->integer('status')->comment('0 - unready : 1 - ready');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
